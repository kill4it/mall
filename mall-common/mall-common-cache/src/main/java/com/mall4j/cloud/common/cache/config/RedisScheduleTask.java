package com.mall4j.cloud.common.cache.config;

import io.seata.common.exception.DataAccessException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

/**
 * @Auther LPJ
 * @Description
 * @Data 2023/2/12 0:23
 * @Version 1.0.0
 **/
@Configuration
@EnableScheduling
public class RedisScheduleTask {
    public static final Log log = LogFactory.getLog(RedisScheduleTask.class);
    
    @Resource
    private StringRedisTemplate dupShowMasterRedisTemplate;
    
    // 1 minutes
    @Scheduled(fixedRate = 60000)
    private void configureTasks() {
        log.debug("ping redis");
        dupShowMasterRedisTemplate.execute(new RedisCallback<String>() {
            @Override
            public String doInRedis(@NotNull RedisConnection connection) throws DataAccessException {
                return connection.ping();
            }
        });
    }
}
