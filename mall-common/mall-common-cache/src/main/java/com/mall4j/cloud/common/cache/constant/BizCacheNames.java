package com.mall4j.cloud.common.cache.constant;

/**
 * @author LPJ
 * @date 2021/01/25
 */
public interface BizCacheNames {

    /**
     * 前缀
     */
    String COUPON_PREFIX = "mall4cloud_biz:";

}
