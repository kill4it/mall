package com.mall4j.cloud.common.mq.config;


import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableRabbit
@Configuration
public class RabbitConfig {


    //1.声明注册fanout模式的交换机
    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("test_direct_exchange", true, false);
    }
    //2.声明队列
    @Bean
    public Queue directSmsQueue(){
        return new Queue("test1.direct.queue",true);
    }
    //3.完成绑定关系
    @Bean
    public Binding directSmsBind() {
        return BindingBuilder.bind(directSmsQueue()).to(directExchange()).with("test1");
    }

}
