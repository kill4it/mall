package com.mall4j.cloud.user.listener;


import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class TestConsumer {

    @RabbitHandler
    @RabbitListener(queues = {"test1.direct.queue"})
    public void reviceMessage(String message, Channel channel, Message messages, @Headers Map<String, Object> headers) {
        System.out.println("接收到的信息是：" + message);
    }
}
