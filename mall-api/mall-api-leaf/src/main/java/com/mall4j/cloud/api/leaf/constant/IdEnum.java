package com.mall4j.cloud.api.leaf.constant;

/**
 * @Auther LPJ
 * @Description
 * @Data 2024/2/19 17:49
 * @Version 1.0.0
 **/
public enum IdEnum {
    
    /**
     * 雪花算法
     */
    SnowFlake,
    /**
     * 日期算法
     */
    ShortCode,
    /**
     * 随机算法
     */
    RandomNumeric;
}
