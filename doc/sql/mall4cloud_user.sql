/*
 Navicat Premium Data Transfer

 Source Server         : 腾讯云
 Source Server Type    : MySQL
 Source Server Version : 80040
 Source Host           : 106.55.191.228:3306
 Source Schema         : mall4cloud_user

 Target Server Type    : MySQL
 Target Server Version : 80040
 File Encoding         : 65001

 Date: 25/10/2024 16:37:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for area
-- ----------------------------
DROP TABLE IF EXISTS `area`;
CREATE TABLE `area`  (
  `area_id` bigint(0) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `area_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '地址',
  `parent_id` bigint(0) NOT NULL COMMENT '上级地址',
  `level` int(0) NOT NULL COMMENT '等级（从1开始）',
  PRIMARY KEY (`area_id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE COMMENT '上级id'
) ENGINE = InnoDB AUTO_INCREMENT = 659041 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '省市区地区信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of area
-- ----------------------------
INSERT INTO `area` VALUES (44, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '广东省', 0, 1);
INSERT INTO `area` VALUES (4401, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '广州市', 44, 2);
INSERT INTO `area` VALUES (4403, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '深圳市', 44, 2);
INSERT INTO `area` VALUES (4406, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '佛山市', 44, 2);
INSERT INTO `area` VALUES (440103, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '荔湾区', 4401, 3);
INSERT INTO `area` VALUES (440104, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '越秀区', 4401, 3);
INSERT INTO `area` VALUES (440105, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '海珠区', 4401, 3);
INSERT INTO `area` VALUES (440106, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '天河区', 4401, 3);
INSERT INTO `area` VALUES (440111, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '白云区', 4401, 3);
INSERT INTO `area` VALUES (440112, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '黄埔区', 4401, 3);
INSERT INTO `area` VALUES (440113, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '番禺区', 4401, 3);
INSERT INTO `area` VALUES (440114, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '花都区', 4401, 3);
INSERT INTO `area` VALUES (440115, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '南沙区', 4401, 3);
INSERT INTO `area` VALUES (440117, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '从化区', 4401, 3);
INSERT INTO `area` VALUES (440118, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '增城区', 4401, 3);
INSERT INTO `area` VALUES (440303, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '罗湖区', 4403, 3);
INSERT INTO `area` VALUES (440304, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '福田区', 4403, 3);
INSERT INTO `area` VALUES (440305, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '南山区', 4403, 3);
INSERT INTO `area` VALUES (440306, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '宝安区', 4403, 3);
INSERT INTO `area` VALUES (440307, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '龙岗区', 4403, 3);
INSERT INTO `area` VALUES (440308, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '盐田区', 4403, 3);
INSERT INTO `area` VALUES (440309, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '龙华区', 4403, 3);
INSERT INTO `area` VALUES (440310, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '坪山区', 4403, 3);
INSERT INTO `area` VALUES (440311, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '光明区', 4403, 3);
INSERT INTO `area` VALUES (440604, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '禅城区', 4406, 3);
INSERT INTO `area` VALUES (440605, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '南海区', 4406, 3);
INSERT INTO `area` VALUES (440606, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '顺德区', 4406, 3);
INSERT INTO `area` VALUES (440607, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '三水区', 4406, 3);
INSERT INTO `area` VALUES (440608, '2024-10-25 06:27:44', '2024-10-25 06:27:44', '高明区', 4406, 3);

-- ----------------------------
-- Table structure for undo_log
-- ----------------------------
DROP TABLE IF EXISTS `undo_log`;
CREATE TABLE `undo_log`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `branch_id` bigint(0) NOT NULL,
  `xid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `context` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `rollback_info` longblob NOT NULL,
  `log_status` int(0) NOT NULL,
  `log_created` datetime(0) NOT NULL,
  `log_modified` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `ux_undo_log`(`xid`, `branch_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of undo_log
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` bigint(0) NOT NULL COMMENT 'ID',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '注册时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户昵称',
  `pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像图片路径',
  `status` int(0) NOT NULL DEFAULT 1 COMMENT '状态 1 正常 0 无效',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------

-- ----------------------------
-- Table structure for user_addr
-- ----------------------------
DROP TABLE IF EXISTS `user_addr`;
CREATE TABLE `user_addr`  (
  `addr_id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '建立时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `user_id` bigint(0) NOT NULL COMMENT '用户ID',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机',
  `is_default` tinyint(0) NOT NULL COMMENT '是否默认地址 1是',
  `consignee` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收货人',
  `province_id` bigint(0) NULL DEFAULT NULL COMMENT '省ID',
  `province` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省',
  `city_id` bigint(0) NULL DEFAULT NULL COMMENT '城市ID',
  `city` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '城市',
  `area_id` bigint(0) NULL DEFAULT NULL COMMENT '区ID',
  `area` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区',
  `post_code` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮编',
  `addr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `lng` decimal(12, 6) NULL DEFAULT NULL COMMENT '经度',
  `lat` decimal(12, 6) NULL DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`addr_id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 206 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户地址' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_addr
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
