package com.mall4j.cloud.demo.thread;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.IntConsumer;

class ZeroEvenOdd {
    private final int n;
    //无锁
    private volatile int state;


    //信号量
    private final Semaphore zeroSema = new Semaphore(1);
    private final Semaphore oddSema = new Semaphore(0);//奇数
    private final Semaphore evenSema = new Semaphore(0);//偶数

    public ZeroEvenOdd(int n) {
        this.n = n;
    }


    //无锁 yield利用线程阻塞
    public void zero(IntConsumer printNumber) throws InterruptedException {
        for (int i = 0; i < n; i++) {
            while (state != 0) {
                Thread.yield();
            }
            if (i < 10) { //小于10 补充0
                printNumber.accept(0);
            }
            if (i % 2 == 0) {
                state = 1;
            } else {
                state = 2;
            }
        }
    }

    public void even(IntConsumer printNumber) throws InterruptedException {
        for (int i = 2; i <= n; i += 2) {
            while (state != 2) {
                Thread.yield();
            }
            printNumber.accept(i);
            state = 0;
        }
    }

    public void odd(IntConsumer printNumber) throws InterruptedException {
        for (int i = 1; i <= n; i += 2) {
            while (state != 1) {
                Thread.yield();
            }
            printNumber.accept(i);
            state = 0;
        }
    }

//    //信号量
//    public void zero(IntConsumer printNumber) throws InterruptedException {
//        for (int i = 1; i <= n; i++) {
//            zeroSema.acquire();
//            if (i < 10) { //小于10 补充0
//                printNumber.accept(0);
//            }
//
//            if ((i & 1) == 1) {//奇数
//                oddSema.release(); //奇数打印
//            } else {
//                evenSema.release(); //偶数打印
//            }
//        }
//    }
//
//    public void even(IntConsumer printNumber) throws InterruptedException {
//        for (int i = 1; i <= n; i++) {
//            if ((i & 1) == 0) {//偶数 打印偶数 并释放zero的线程
//                evenSema.acquire();
//                printNumber.accept(i);
//                zeroSema.release();
//            }
//        }
//    }
//
//    public void odd(IntConsumer printNumber) throws InterruptedException {
//        for (int i = 1; i <= n; i++) {
//            if ((i & 1) == 1) {//奇数，打印奇数，并释放zero的线程
//                oddSema.acquire();
//                printNumber.accept(i);
//                zeroSema.release();
//            }
//        }
//    }


    public static void main(String[] args) {
        ZeroEvenOdd zeroEvenOdd = new ZeroEvenOdd(30);
        IntConsumer printNumber = new IntConsumer() {
            @Override
            public void accept(int value) {
                System.out.println(value);
            }
        };
        new Thread(() -> {
            try {
                zeroEvenOdd.zero(printNumber);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                zeroEvenOdd.even(printNumber);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                zeroEvenOdd.odd(printNumber); //执行runable
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

    }
}


