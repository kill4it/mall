package com.mall4j.cloud.demo.demo.juc;

/**
 * volatile
 * @Auther LPJ
 * @Description
 * @Data 2022/9/7 22:50
 * @Version 1.0.0
 **/
public class Singleton {
    private volatile static Singleton instance;
    private Object lock;
    private Singleton (){}
    public static Singleton getInstance() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}
