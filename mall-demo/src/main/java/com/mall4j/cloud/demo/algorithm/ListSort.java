package com.mall4j.cloud.demo.algorithm;

import java.util.Arrays;

/**
 * @Auther LPJ
 * @Description
 * @Data 2024/4/13 16:23
 * @Version 1.0.0
 **/
public class ListSort {
    public static void main(String[] args) {
        //生成一个乱序数组
        int[] arr = new int[]{3, 5, 10, 7, 9, 3, 4, 6, 1};
        
        //insertSort(arr);
        //shellSort(arr);
        mergeSort(arr, 0, arr.length - 1);
        for (int i : arr) {
            System.out.println(i);
        }
    }
    
    //写一个冒泡排序
    public static void bubbleSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }
    
    //写一个选择排序
    public static void selectSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }
            int temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;
        }
    }
    
    //插入排序
    public static void insertSort(int[] arr) {
        
        for (int i = 0; i < arr.length; i++) {
            int current = arr[i];
            int j = i - 1;
            while (j >= 0 && current < arr[j]) {//遍历左侧数据 arr[0...i-1]
                //当满足条件时 ,左侧全部右移一位, 为要插入的数据腾出位置
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = current; //插入到左侧数据腾出的位置
        }
        
    /*
           //自己第一次写的
        for (int i = 1; i < arr.length; i++) {
            int minIndex = i;
            while (minIndex - 1 >= 0 && arr[minIndex] > arr[minIndex - 1]) {                //直到找到比最小值小的
                int temp = arr[minIndex];
                arr[minIndex] = arr[minIndex - 1];
                arr[minIndex - 1] = temp;
                minIndex--;
            }
        }*/
        
        
    }
    
    //写一个希尔排序
    public static void shellSort(int[] arr) {
        int gap = arr.length / 2;
        while (gap > 0) {
            for (int i = 0; i < arr.length; i++) {
                int currentValue = arr[i];
                int j = i - gap;
                while (j >= 0 && currentValue < arr[j]) {
                    arr[j + gap] = arr[j];
                    j = j - gap;
                }
                arr[j + gap] = currentValue;
            }
            gap = gap / 2;
        }
        
        
       /* int gap = arr.length / 2;
        while (gap > 0) {
            //插入排序
            for (int i = gap; i < arr.length; i++) {
                int current = arr[i];
                int j = i - gap;
                while (j >= 0 && current < arr[j]) {
                    arr[j + gap] = arr[j];
                    j -= gap;
                }
                arr[j + gap] = current;
            }
            gap /= 2;
        }*/
    }
    //写一个归并排序
    /**
     * 合并两个已排序的子数组
     *
     * @param arr   原始数组
     * @param left  第一个子数组起始索引
     * @param mid   分割点（第一个子数组结束，第二个子数组开始）
     * @param right 第二个子数组结束索引
     */
    private static void merge(int[] arr, int left, int mid, int right) {
        int[] temp = new int[right - left + 1]; // 创建临时数组存放合并结果
        int i = left, j = mid + 1, k = 0;
        
        // 同时遍历两个子数组，将较小元素放入临时数组
        while (i <= mid && j <= right) {
            temp[k++] = arr[i] <= arr[j] ? arr[i++] : arr[j++];
        }
        
        // 若左侧子数组还有剩余元素，将其添加到临时数组
        while (i <= mid) {
            temp[k++] = arr[i++];
        }
        
        // 若右侧子数组还有剩余元素，将其添加到临时数组
        while (j <= right) {
            temp[k++] = arr[j++];
        }
        
        // 将临时数组中的元素复制回原数组
        System.arraycopy(temp, 0, arr, left, temp.length);
    }
    
    /**
     * 归并排序主函数
     *
     * @param arr 待排序数组
     * @param left  起始索引
     * @param right 结束索引
     */
    public static void mergeSort(int[] arr, int left, int right) {
        if (left < right) {
            int mid = left + (right - left) / 2; // 计算中间索引
            
            // 递归对左右两个子数组进行归并排序
            mergeSort(arr, left, mid);
            mergeSort(arr, mid + 1, right);
            
            // 合并已排序的子数组
            merge(arr, left, mid, right);
        }
    }
    
    //快速排序
    //选择基准值：从待排序的数组中选取一个元素作为基准值。
    //分区：遍历数组，将所有元素与基准值比较并重新排列，使得基准值左边的元素都不大于它，右边的元素都不小于它。
    //递归排序：对基准值左右两侧的子数组进行递归的快速排序，直到所有子数组长度为0或1（已有序）。
    public static void quickSort(int[] arr, int low, int high) {
        if (low < high) {
            // 找到分区点，将数组分为两部分
            int pivotIndex = partition(arr, low, high);
        
            // 对左子数组进行递归排序
            quickSort(arr, low, pivotIndex - 1);
        
            // 对右子数组进行递归排序
            quickSort(arr, pivotIndex + 1, high);
        }
    }
    private static int partition(int[] arr, int low, int high) {
        // 选择基准值，这里选择数组的最后一个元素
        int pivot = arr[high];
        
        // i指向小于基准值的元素的边界（初始时为low-1）
        int i = low - 1;
        
        for (int j = low; j < high; j++) {
            // 如果当前元素小于或等于基准值
            if (arr[j] <= pivot) {
                i++;
                
                // 交换arr[i]和arr[j]
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
        
        // 将基准值放到正确的位置（即i+1处）
        int temp = arr[i + 1];
        arr[i + 1] = arr[high];
        arr[high] = temp;
        
        // 返回分区点（基准值的新位置）
        return i + 1;
    }
    
    
    
    
    
    
    
    
    
}
