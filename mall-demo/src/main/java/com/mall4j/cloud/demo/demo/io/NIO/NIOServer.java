package com.mall4j.cloud.demo.demo.io.NIO;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * NIO服务端
 *
 * @Auther LPJ
 * @Description
 * @Data 2022/7/31 19:11
 * @Version 1.0.0
 **/
public class NIOServer {
    private int port = 8080;
    //轮询器 select 轮询处理
    private Selector selector;
    
    
    public NIOServer(int port) throws IOException {
        this.port = port;
        //初始化一个通道
        ServerSocketChannel server = ServerSocketChannel.open();
        //绑定ip：端口号
        server.bind(new InetSocketAddress(this.port));
        //设置为非阻塞。默认是阻塞的
        server.configureBlocking(false);
        
        //初始化一个轮询器---
        selector = Selector.open();
        //在门口翻牌子---正在营业
        server.register(selector, SelectionKey.OP_ACCEPT);
        
        //初始化完毕
    }
    
    public void listen() throws IOException {
        System.out.println("listen on " + this.port + "。");
        while (true) {
            int select = selector.select();//获取更新就绪操作集的键的数量，可能为零
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            //轮询叫号
            Iterator<SelectionKey> iterator = selectionKeys.iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                iterator.remove();
                process(key);
                
            }
        }
    }
    
    //每一次轮询调用一次process方法，不会进行阻塞，但会返回一个状态值
    private void process(SelectionKey key) throws IOException {
        //根据每一种状态给一个反应
        if (key.isAcceptable()) {//数据是否准备就绪
            //数据准备就绪
            ServerSocketChannel server = (ServerSocketChannel) key.channel();
            SocketChannel channel = server.accept();
            channel.configureBlocking(false);
            //将数据状态改为可读read
            key = channel.register(selector, SelectionKey.OP_READ);
        } else if (key.isReadable()) {//可读
            //通过key 反向获取到对应的channel
            SocketChannel channel = (SocketChannel) key.channel();
            //获取到该channel关联的buffer
            ByteBuffer buffer = (ByteBuffer) key.attachment();
            while (channel.read(buffer) != -1) {
                //读取buffer中的数据
                buffer.flip();
                String readed = new String(buffer.array(), 0, buffer.limit());
                System.out.println(readed);
                buffer.clear();
                //修改标志位
                key = channel.register(selector, SelectionKey.OP_WRITE);
                //携带一个附件
                key.attach(readed);
            }
        } else if (key.isWritable()) {//可写
            //通过key 反向获取到对应的channel
            SocketChannel channel = (SocketChannel) key.channel();
            String attachment = (String) key.attachment();
            channel.write(ByteBuffer.wrap(("输出：" + attachment).getBytes()));
            channel.close();
        }
    }
    
    public static void main(String[] args) throws IOException {
        new NIOServer(8081).listen();
    }
    
}
