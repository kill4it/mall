package com.mall4j.cloud.demo.designmodel.decorator;

/**
 * @Auther LPJ
 * @Description
 * @Data 2023/2/8 18:11
 * @Version 1.0.0
 **/
public class ShapeDecorator implements Shape {
    
    private final Shape shape;
    
    public ShapeDecorator(Shape shape) {
        this.shape = shape;
    }
    
    @Override
    public void draw() {
        shape.draw();
    }
}
