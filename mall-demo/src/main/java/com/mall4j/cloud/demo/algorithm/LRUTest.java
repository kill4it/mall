package com.mall4j.cloud.demo.algorithm;

import java.util.HashMap;
import java.util.Map;

/**
 * @Auther LPJ
 * @Description
 * @Data 2024/9/6 13:57
 * @Version 1.0.0
 **/
public class LRUTest<K, V> {
    private Map<K, Node<K, V>> map = new HashMap<>();//
    private Node<K, V> head;
    private Node<K, V> tail;
    private int capacity;
    
    public LRUTest(int capacity) {
        this.capacity = capacity;
    }
    
    public V get(K key) {
        if (map.containsKey(key)) {
            //存在
            Node<K, V> kvNode = map.get(key);
            //删除node
            removeNode(kvNode);
            //设置到最近使用的位置(头结点)
            setHead(kvNode);
            return kvNode.value;
        }
        
        return null;
    }
    
    public void put(K key, V value) {
        //判断是否存在
        if (map.containsKey(key)) {
            //存在
            Node<K, V> oldNode = map.get(key);
            oldNode.value = value;
            //删除
            removeNode(oldNode);
            //放到头结点上
            setHead(oldNode);
        } else {
            //不存在
            //判断缓存是否已满
            if (map.size() == capacity) {
                //删除最不常使用的节点tail
                map.remove(tail.key);
                //删除tail节点
                removeTail();
            }
            //放到头结点
            Node<K, V> newNode = new Node<>(key, value);
            map.put(key, newNode);
            setHead(newNode);
        }
    }
    
    private void removeTail() {
        tail = tail.pre;
        if (tail != null) {
            tail.next = null;
        }
        
    }
    
    private void setHead(Node<K, V> kvNode) {
        kvNode.pre = null;
        kvNode.next = head;
        if (head != null) {
            head.pre = kvNode;
        }
        head = kvNode;
        if (tail == null) {
            //首次set为空
            tail = head;
        }
    }
    
    private void removeNode(Node<K, V> node) {
        //处理前驱节点
        if (node.pre != null) {
            node.pre.next = node.next;
        } else {
            head = node.next;
        }
        //处理后继节点
        if (node.next != null) {
            node.next.pre = node.pre;
        } else {
            tail = node.pre;
        }
    }
    
    
    private static class Node<K, V> {
        K key;
        V value;
        Node<K, V> pre;
        Node<K, V> next;
        
        Node(K k, V v) {
            this.key = k;
            this.value = v;
        }
        
    }
    
    public void print() {
        Node j = head;
        while (j != null) {
            System.out.println("K :" + j.key.toString() + "V :" + j.value.toString());
            j = j.next;
        }
    }
    
    public static void main(String[] args) {
        LRUTest lruCache = new LRUTest(5);
        for (int i = 0; i < 6; i++) {
            lruCache.put(i, i);
        }        lruCache.print();
        lruCache.get(1);
        lruCache.print();
    }
    
}
