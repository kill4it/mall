package com.mall4j.cloud.demo.demo.juc;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/9/8 18:46
 * @Version 1.0.0
 **/
public class SemaphoreDemo {
    private static Semaphore semaphore = new Semaphore(20);
    
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(200);
        //模拟300辆车
        for (int i = 0; i < 30; i++) {
            executorService.execute(new Runnable() {
                @Override
                public void run() {

                    
                    System.out.println("====" + Thread.currentThread().getName() + "准备进入停车场==");
//                    //车位判断
//                    if (semaphore.availablePermits() == 0) {
//                        System.out.println("车辆不足，请耐心等待");
//                    }
                    
                    try {
                        semaphore.acquire();//信号量不足时会阻塞
                        System.out.println("====" + Thread.currentThread().getName() + "成功进入停车场");
                        //模拟车辆在停车场停留的时间
                        Thread.sleep(new Random().nextInt(2000));
                        
                        System.out.println("====" + Thread.currentThread().getName() + "驶出停车场");
                        //释放令牌，腾出停车场车位
                        semaphore.release();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    
        //线程池关闭
        executorService.shutdown();
        
    }
    
}
