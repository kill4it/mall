package com.mall4j.cloud.demo.designmodel.listener;


/**
 * 监听器事件
 * @Auther LPJ
 * @Description
 * @Data 2022/12/25 0:04
 * @Version 1.0.0
 **/
public class Event {
    private String data;
    private String type;
    Event(String data, String type) {
        this.data = data;
        this.type = type;
    }
    
    public String getData() {
        return data;
    }
    
    public void setData(String data) {
        this.data = data;
    }
    
    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
}
