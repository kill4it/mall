package com.mall4j.cloud.demo.demo.rpc.simple.provider;



import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 模拟我们的远程要调用的服务
 * @Auther LPJ
 * @Description
 * @Data 2022/9/26 19:44
 * @Version 1.0.0
 **/
public class Provider {
    
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(8888);
            System.out.println("启动远程服务监听...");
            //监听客户端发来消息
            while (true) {
                Socket socket = serverSocket.accept();
                ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                //读取客户端传输协议包
                String className = objectInputStream.readUTF();
                String methodName = objectInputStream.readUTF();
                Class<?>[] parameterTypes = (Class<?>[]) objectInputStream.readObject();
                Object[] arguments = (Object[]) objectInputStream.readObject();
                
                Class clazz = null;
                //服务注册：API到具体实现的映射
                //服务提供端可以将api实现类注册到spring容器中，然后这里可以根据类型进行依赖查找
                //Class clazz = Class.forName(className);
                if (className.equals(EchoService.class.getName())) {
                    clazz = EchoServiceImpl.class;
                }
                //传入方法名，方法参数类型获得方法对象
                Method method = clazz.getMethod(methodName, parameterTypes);
                //传入实现类对象，方法参数数组，方法对象执行获得返回结果对象
                Object result = method.invoke(clazz.newInstance(), arguments);
                
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                objectOutputStream.writeObject(result);
                objectOutputStream.flush();
                
                objectInputStream.close();
                objectOutputStream.close();
                socket.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
