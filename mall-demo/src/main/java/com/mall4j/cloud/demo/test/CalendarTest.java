package com.mall4j.cloud.demo.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalendarTest {
    public static void main(String[] args) {

//        Date date = new Date();
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date);
//        for (int i = 0; i < 10; i++) {
//            System.out.println(calendar.getTime());
//            calendar.add(Calendar.SECOND, 1);
//        }

//        Calendar currentTime = getCurrentTime();
//        currentTime.add(Calendar.HOUR, -2); //查询两小时前开始的活动
//        Date time = currentTime.getTime();
//
//
//        System.out.println(time);


        // 使用Calendar类进行日期时间操作
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(new Date()); // 设置calendar到beginDateTime时间
//        calendar.add(Calendar.HOUR_OF_DAY, -24); // 减去24小时
//        // 获取24小时前的日期时间
//        Date twentyFourHoursBefore = calendar.getTime();
//        System.out.println(twentyFourHoursBefore);
//        calendar.setTime(new Date());
//        calendar.set(Calendar.SECOND, 0);
//        calendar.set(Calendar.MILLISECOND, 0);
//
//        Date nowTime = calendar.getTime();
//        System.out.println(nowTime);

        // 获取当前时间
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startTime = getStartTime("2022-12-15","yyyy-MM-dd");
        Date endTime = getEndTime("2022-12-15","yyyy-MM-dd");
        String format = sdf.format(startTime);
        System.out.println(format);
        String endTimeStr = sdf.format(endTime);
        System.out.println(endTimeStr);

    }


    private static Calendar getCurrentTime() {


        // 获取当前时间
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }


    public static Date getStartTime(String currentTime, String format) {
        try {
            // 将字符串转换为 Date
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Date currentDateTime = sdf.parse(currentTime);

            // 创建 Calendar 实例
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(currentDateTime);

            // 设置时间为当天的开始时间（00:00:00）
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            return calendar.getTime();
        } catch (ParseException e) {
            throw new IllegalArgumentException("Invalid date format", e);
        }
    }


    public static Date getEndTime(String currentTime, String format) {
        try {
            // 将字符串转换为 Date
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Date currentDateTime = sdf.parse(currentTime);

            // 创建 Calendar 实例
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(currentDateTime);

            // 设置时间为当天的结束时间（23:59:59.999）
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 999);

            return calendar.getTime();
        } catch (ParseException e) {
            throw new IllegalArgumentException("Invalid date format", e);
        }
    }
}
