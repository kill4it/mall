package com.mall4j.cloud.demo.thread;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class FooBar {

    private final int n;
    //锁
    private final Object lock = new Object();

    private final int state = 0;
//信号量
    private final Semaphore fooSemaphore = new Semaphore(1);
    private final Semaphore barSemaphore = new Semaphore(0);


    public FooBar(int n) {
        this.n = n;
    }

//    public void foo(Runnable printFoo) throws InterruptedException {
//
//        for (int i = 0; i < n; i++) {
//            synchronized (lock) {
//                while (state != 0) {
//                    lock.wait();
//                }
//                // printFoo.run() outputs "foo". Do not change or remove this line.
//                printFoo.run();
//                state = 1;
//                lock.notifyAll();
//            }
//
//
//        }
//    }

    //信号量
    public void foo(Runnable printFoo) throws InterruptedException {
        for (int i = 0; i < n; i++) {
            fooSemaphore.acquire(); // Acquire the semaphore for "foo"

            // Print "foo"
            printFoo.run();

            barSemaphore.release(); // Release the semaphore for "bar"
        }
    }



//    public void bar(Runnable printBar) throws InterruptedException {
//
//        for (int i = 0; i < n; i++) {
//            synchronized (lock) {
//                while (state != 1) {
//                    lock.wait();
//                }
//                // printBar.run() outputs "bar". Do not change or remove this line.
//                printBar.run();
//                state = 0;
//                lock.notifyAll();
//            }
//        }
//    }

    //信号量
    public void bar(Runnable printBar) throws InterruptedException {
        for (int i = 0; i < n; i++) {
            barSemaphore.acquire(); // Acquire the semaphore for "bar"

            // Print "bar"
            printBar.run();

            fooSemaphore.release(); // Release the semaphore for "foo"
        }
    }

    public static void main(String[] args) {
        ExecutorService threadPool = Executors.newFixedThreadPool(2);
        FooBar fooBar = new FooBar(10);
        Runnable foo = () -> System.out.print("foo");
        Runnable bar = () -> System.out.println("bar");
        new Thread(() -> {
            try {
                fooBar.foo(foo); //执行runable
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                fooBar.bar(bar);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
