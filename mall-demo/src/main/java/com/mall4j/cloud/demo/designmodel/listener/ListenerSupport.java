package com.mall4j.cloud.demo.designmodel.listener;

import java.util.ArrayList;
import java.util.List;

/**
 * 事件管理器
 * @Auther LPJ
 * @Description
 * @Data 2022/12/25 0:10
 * @Version 1.0.0
 **/
public class ListenerSupport {
    private final List<Listener> listeners = new ArrayList<>();
    
    /**
     * 添加监听者
     * @param listener
     */
    public void addListener(Listener listener) {
        listeners.add(listener);
    }
    
    /**
     * 触发事件
     * @param event
     */
    public void triggerEvent(Event event) {
        for (Listener listener : listeners) {
            listener.onClick(event);
        }
    }
}
