package com.mall4j.cloud.demo.demo.proxy.cglib;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

class CglibSqlInterceptor implements MethodInterceptor {
    private final UserDao userDao;

    public CglibSqlInterceptor(UserDao userDao){
        this.userDao = userDao;
    }

    @Override
    public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        System.out.println("before.....");
        Object result = methodProxy.invoke(userDao,args);
        System.out.println("after.....");
        return result;
    }
}

