package com.mall4j.cloud.demo.designmodel.listener;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/12/25 0:07
 * @Version 1.0.0
 **/
public class ListenerA implements Listener {
    @Override
    public void onClick(Event event) {
        //可以针对event做判断
        if ("typeA".equals(event.getType())) {
            System.out.println("触发事件，type:" + event.getType() + "，data:" + event.getData());
        }
    }
    
}
