package com.mall4j.cloud.demo.lang;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/10/8 21:38
 * @Version 1.0.0
 **/
public class String {
    static {
        System.out.println("hello,String");
    }
    //错误: 在类 java.lang.String 中找不到 main 方法
    public static void main(java.lang.String[] args) {
        System.out.println("hello,String");
    }

}
