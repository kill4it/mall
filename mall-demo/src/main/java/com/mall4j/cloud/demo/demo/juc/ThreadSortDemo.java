package com.mall4j.cloud.demo.demo.juc;

/**
 * 如何线程顺序执行
 * @Auther LPJ
 * @Description
 * @Data 2022/9/8 20:36
 * @Version 1.0.0
 **/
public class ThreadSortDemo {
    public static void main(String[] args) throws InterruptedException {
        Thread spring = new Thread(new SeasonThreadTask("春天"));
        Thread summer = new Thread(new SeasonThreadTask("夏天"));
        Thread autumn = new Thread(new SeasonThreadTask("秋天"));

        spring.start();
        spring.join(1000);//等待join()返回后才继续执行
        summer.start();
        summer.join();
        autumn.start();
        autumn.join();
    }
}
class SeasonThreadTask implements Runnable{
    
    private String name;
    
    public SeasonThreadTask(String name){
        this.name = name;
    }
    
    @Override
    public void run() {
        for (int i = 1; i <4; i++) {
            System.out.println(this.name + "来了: " + i + "次");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}