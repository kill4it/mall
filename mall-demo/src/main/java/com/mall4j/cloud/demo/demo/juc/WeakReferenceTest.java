package com.mall4j.cloud.demo.demo.juc;

import java.lang.ref.WeakReference;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/9/8 12:28
 * @Version 1.0.0
 **/
public class WeakReferenceTest {
    public static void main(String[] args) {
        Object object = new Object();
        //WeakReference是一个强引用，内部才会存在一个弱引用
        WeakReference<Object> testWeakReference = new WeakReference<>(object);
        System.out.println("GC回收之前，弱引用：" + testWeakReference.get());
        System.out.println("testWeakReference：" + testWeakReference);
        //触发系统垃圾回收
        System.gc();
        System.out.println("GC回收之后，弱引用：" + testWeakReference.get());
        System.out.println("testWeakReference：" + testWeakReference);
        //手动设置为object对象为null
        object = null;
        System.gc();
        System.out.println("对象object设置为null，GC回收之后，弱引用：" + testWeakReference.get());
        System.out.println("testWeakReference：" + testWeakReference);
        
        
    }
    
}
