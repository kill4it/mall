package com.mall4j.cloud.demo.demo.classTest.reflect;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/10/16 10:35
 * @Version 1.0.0
 **/
public class Student {
    public String name;
    private int age;
    public static final Integer INT_VALUE = 100;
    
    public static int nums = 100;
    public Student() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public Student(String name, int age) {
        super();
        this.name = name;
        this.age = age;
    }
    
    public void name() {
        System.out.println("测试");
        
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getAge() {
        return age;
    }
    
    public void setAge(int age) {
        this.age = age;
    }
    
    private void mane1() {
        System.out.println("这是个萌萌哒私有的");
        
    }
    
    @Override
    public String toString() {
        return "Student01 [name=" + name + ", age=" + age + "]";
    }
}
