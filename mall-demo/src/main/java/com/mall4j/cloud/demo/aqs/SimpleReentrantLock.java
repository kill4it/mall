package com.mall4j.cloud.demo.aqs;

import java.util.concurrent.locks.AbstractQueuedSynchronizer;

public class SimpleReentrantLock {

    private final Sync sync;

    public SimpleReentrantLock(boolean fair) {
        sync = fair ? new FairSync() : new NonfairSync();
    }

    public void lock() {
        sync.lock();
    }

    public boolean tryLock() {
        return sync.tryLock();
    }

    public void unlock() {
        sync.unlock();
    }

    private abstract static class Sync extends AbstractQueuedSynchronizer {

        private static final long serialVersionUID = 1L;

        protected Sync() {
            super();
        }

        protected final boolean isHeldExclusively() {
            return getState() != 0;
        }

        @Override
        protected boolean tryAcquire(int arg) {
            Thread current = Thread.currentThread();
            int c = getState();
            if (c == 0) {
                if (compareAndSetState(0, 1)) {
                    setExclusiveOwnerThread(current);
                    return true;
                }
            } else if (current == getExclusiveOwnerThread()) {
                int nextC = c + 1;
                setState(nextC);
                return true;
            }
            return false;
        }

        @Override
        protected boolean tryRelease(int arg) {
            Thread current = Thread.currentThread();
            int c = getState();
            if (c == 0 || current != getExclusiveOwnerThread()) {
                throw new IllegalMonitorStateException();
            }
            int nextC = c - 1;
            if (nextC == 0) {
                setState(nextC);
                setExclusiveOwnerThread(null);
            } else {
                setState(nextC);
            }
            return true;
        }




        public abstract void lock();

        public abstract boolean tryLock();

        public abstract void unlock();
    }

    private static class FairSync extends Sync {
        private static final long serialVersionUID = 1L;

        @Override
        public void lock() {
            acquire(1);
        }

        @Override
        public boolean tryLock() {
            return tryAcquire(1);
        }

        @Override
        public void unlock() {
            tryRelease(0);
        }
    }

    private static class NonfairSync extends Sync {
        private static final long serialVersionUID = 1L;

        @Override
        public void lock() {
            if (compareAndSetState(0, 1)) {
                setExclusiveOwnerThread(Thread.currentThread());
                return;
            }
            acquire(1);
        }

        @Override
        public boolean tryLock() {
            return tryAcquire(1);
        }

        @Override
        public void unlock() {
            tryRelease(1);
        }
    }


    public static void main(String[] args) {
        SimpleReentrantLock lock = new SimpleReentrantLock(false); // 创建非公平锁

        Thread thread1 = new Thread(() -> {
            lock.lock();
            try {
                System.out.println("Thread 1 locked.");
                Thread.sleep(1000);
                System.out.println("Thread 1 releasing lock.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        });

        Thread thread2 = new Thread(() -> {
            lock.lock();
            try {
                System.out.println("Thread 2 locked.");
                Thread.sleep(1000);
                System.out.println("Thread 2 releasing lock.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        });

        thread1.start();
        thread2.start();
    }
}
