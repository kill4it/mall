package com.mall4j.cloud.demo.demo.rpc.simple.consumer;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.Socket;

/**
 * 服务消费者
 * 主要就是利用 jdk 的动态代理，创建一个代理类，在执行目标方法时会调用 invoke 方法，方法内部通过 socket 连接服务端，
 * 利用 java 反射获取 api 相关类名、方法名等并作为参数发送给服务端，并等待服务端的响应。
 * @Auther LPJ
 * @Description
 * @Data 2022/9/26 19:20
 * @Version 1.0.0
 **/
public class Client {

    
    /**
     * rpc
     * 动态创建代理对象
     * Object newProxyInstance(ClassLoader loader, Class<?>[] interfaces, InvocationHandler h)
     * 参数1：真实对象的类加载器
     * 参数2：真实对象实现的所有的接口,接口是特殊的类，使用Class[]装载多个接口
     * 参数3： 接口，传递一个匿名内部类对象
     *
     * @param clazz clazz
     * @return {@link Object}
     */
    public static Object rpc(final Class clazz) {
        return Proxy.newProxyInstance(clazz.getClassLoader(), new Class[]{clazz}, new InvocationHandler() {
            /*
             * @param proxy  代理对象
             * @param method    代理的方法对象
             * @param args  方法调用时参数
             * @return
             * @throws Throwable
             */
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                Socket socket = new Socket("127.0.0.1", 8888);
                String className = clazz.getName();//api类名
                String methodName = method.getName();//api 类成员方法名
                Class<?>[] parameterTypes = method.getParameterTypes(); //类成员方法参数类型集合
                //可以扩展使用具体的 寻址、协议、负载均衡、序列化、
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                objectOutputStream.writeUTF(className);
                objectOutputStream.writeUTF(methodName);
                objectOutputStream.writeObject(parameterTypes);
                objectOutputStream.writeObject(args);
                ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                Object o = objectInputStream.readObject();
                objectInputStream.close();
                objectOutputStream.close();
                return o;
            }
        });
    }
}
