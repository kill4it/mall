package com.mall4j.cloud.demo.demo.proxy.jdk;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/9/13 18:48
 * @Version 1.0.0
 **/
public interface UserService {
    void addUser();
    
    void updateUser(String str);
}
