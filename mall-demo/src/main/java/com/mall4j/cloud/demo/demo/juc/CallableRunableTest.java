package com.mall4j.cloud.demo.demo.juc;

import java.util.concurrent.*;

/**
 * 面试题：Callable和Runable
 * Callable可以抛异常和阻塞获取返回值
 * @Auther LPJ
 * @Description
 * @Data 2022/9/7 22:17
 * @Version 1.0.0
 **/
public class CallableRunableTest {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
    
        Callable<String> callable =new Callable<String>() {
            @Override
            public String call() throws Exception {
                Thread.sleep(10000);//block 10s
                return "你好，callable,关注公众号：捡田螺的小男孩";
            }
        };
        //支持泛型
        Future<String> futureCallable = executorService.submit(callable);
    
        try {
            //get()方法会阻塞等待返回值
            System.out.println("The callable return results："+futureCallable.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    
        //Runable
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(10000);//block 10s
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("你好呀,runnable，关注公众号：捡田螺的小男孩");
            }
        };
    
        Future<?> futureRunnable = executorService.submit(runnable);
        try {
            //get()方法会阻塞等待返回值
            System.out.println("获取runnable的返回结果："+futureRunnable.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
    }
}
