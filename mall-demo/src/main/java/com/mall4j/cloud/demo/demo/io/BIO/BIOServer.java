package com.mall4j.cloud.demo.demo.io.BIO;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/7/31 18:34
 * @Version 1.0.0
 **/
public class BIOServer {
    
    ServerSocket server;
    
    public BIOServer(int port) {
        try {
            server = new ServerSocket(port);
            System.out.println("BIO服务器已启动" + port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    //监听并处理
    public void listen() throws IOException {
        while (true) {
            //阻塞获取数据。。。。。
            Socket client = server.accept();
            //打印客户端发送数据的端口号
            System.out.println("客户端端口号:" + client.getPort());
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        int count = 0;
                        while (!client.isClosed() && count < 20) {
                            try {
                                client.getOutputStream().write(String.format("服务器收到%d", count).getBytes(StandardCharsets.UTF_8));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            count++;
                        }
                    } finally {
                        try {
                            client.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    
                }
            }).start();
            
        }
    }
    
    public static void main(String[] args) throws IOException {
        new BIOServer(8087).listen();
    }
    
    
}
