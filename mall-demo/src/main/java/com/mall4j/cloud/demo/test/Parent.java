package com.mall4j.cloud.demo.test;

/**
 * @Auther LPJ
 * @Description
 * @Data 2023/3/26 19:20
 * @Version 1.0.0
 **/
public class Parent {
    public String name = "父亲";
    public Integer money = 100;
    
    public String getName() {
        return name;
    }
    
    public Integer getMoney() {
        return money;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setMoney(Integer money) {
        this.money = money;
    }
    
}
