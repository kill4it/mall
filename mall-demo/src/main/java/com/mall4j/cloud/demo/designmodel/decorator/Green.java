package com.mall4j.cloud.demo.designmodel.decorator;

/**
 * @Auther LPJ
 * @Description
 * @Data 2023/2/8 18:13
 * @Version 1.0.0
 **/
public class Green extends ShapeDecorator {
    
    public Green(Shape shape) {
        super(shape);
    }
    
    @Override
    public void draw() {
        super.draw();
        System.out.print(" 绿色");
    }
}
