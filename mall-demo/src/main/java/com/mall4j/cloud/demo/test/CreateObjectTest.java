package com.mall4j.cloud.demo.test;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/11/11 15:17
 * @Version 1.0.0
 **/
class ReflectDemo {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        proxyObject();
        newObject();
    }
    
    //new 创建对象
    //5
    private static void newObject() {
        long startTime = System.currentTimeMillis();
        int i;
        for (i = 0; i < 100000000; i++) {
            ReflectDemo reflectDemo = new ReflectDemo();
        }
        if (i == 100000000) {
            long endTime = System.currentTimeMillis();
            System.out.println("new耗时为:" + (endTime - startTime));
        }
    }
    
    //反射 创建对象
    //30
    private static void proxyObject() throws InstantiationException, IllegalAccessException {
        long startTime = System.currentTimeMillis();
        Class<ReflectDemo> reflectDemoClass = ReflectDemo.class;
        int i;
        for (i = 0; i < 100000000; i++) {
            ReflectDemo reflectDemo = reflectDemoClass.newInstance();
        }
        if (i == 100000000) {
            long endTime = System.currentTimeMillis();
            System.out.println("反射耗时为:" + (endTime - startTime));
        }
    }
}
    

