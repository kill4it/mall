package com.mall4j.cloud.demo.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 集合相关操作
 * @Auther LPJ
 * @Description
 * @Data 2024/2/22 16:37
 * @Version 1.0.0
 **/
public class CollecitionTest {
    
    
    
    
    public static void main(String[] args) {
        List list = new ArrayList<>();
        list.add(1);
        list.add("2");
        Iterator iterator = list.iterator();

        while (iterator.hasNext()) {

            System.out.println(iterator.next());
            iterator.remove();
        }
        example2();
        System.out.println(System.currentTimeMillis());

    }
    
    
    public static void SafeFailureExample(){
        // 创建一个 CopyOnWriteArrayList 实例
        CopyOnWriteArrayList<String> list = new CopyOnWriteArrayList<>();
        list.add("Apple");
        list.add("Banana");
        list.add("Cherry");
        // 创建一个线程来修改列表
        Thread updaterThread = new Thread(() -> {
            list.add("Date"); // 在其他线程中添加元素
        });
        // 启动更新线程
        updaterThread.start();
        // 使用 Iterator 遍历列表，即使在遍历过程中有其他线程修改了集合
        for (Iterator<String> iterator = list.iterator(); iterator.hasNext();) {
            System.out.println(iterator.next()); // 不会抛出 ConcurrentModificationException
        }
    
        // 确保更新线程完成
        try {
            updaterThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void example2(){
        List<String> uids = new ArrayList<>();
        uids.add("18888");
        uids.add("19998");
        uids.add("29998");
        String join = String.join("_", uids);
        System.out.println(join);
    }
    
    
    
}
