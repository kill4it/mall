package com.mall4j.cloud.demo.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
/**
 * @Desc： 现有三组字符串：ABC/DEF/GHI，要求用线程打印出A D G B E H C F I
 */
public class InterleavedStringPrinter {
    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        ExecutorService threadPool = Executors.newFixedThreadPool(3);

        threadPool.execute(new MyRunnable2("ABC", lock, 0));
        threadPool.execute(new MyRunnable2("DEF", lock, 1));
        threadPool.execute(new MyRunnable2("GHI", lock, 2));
        threadPool.shutdown();
    }



}

class MyRunnable2 implements Runnable {

    private final String str;

    private final Lock lock;

    private final int threadId;

    private static int count = 0;

    private static final int MAX = 9;

    public MyRunnable2(String str, Lock lock, int threadId) {
        this.str = str;
        this.lock = lock;
        this.threadId = threadId;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (lock){
                // 为什么在这加判断，而不是在while里面写count<=MAX，因为那样的话会打印11次。
                if (count >= MAX) {
                    return;
                }
                if (count % 3 == threadId) { //012 012 012 具体该那个线程执行
                    int index = count / 3; // 0 1 2  字符串位置
                    System.out.print(str.charAt(index) + " ");
                    count ++ ;
                }
            }
        }
    }
}
