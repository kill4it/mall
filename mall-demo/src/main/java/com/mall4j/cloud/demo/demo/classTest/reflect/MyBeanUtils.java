package com.mall4j.cloud.demo.demo.classTest.reflect;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/10/16 12:53
 * @Version 1.0.0
 **/
public class MyBeanUtils {
    //因为是工具类，不需要实例化。所以私有构造方法
    private MyBeanUtils() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    /*
     * 给对象中的属性赋值 传入类的对象类型，和要修改的属性的值不确定所以用Object类型，属性名用String类型
     */
    public static void setProrerty(Object object, String name, Object values)
            throws ReflectiveOperationException, SecurityException {
        // 获取传入对象的字节码文件
        Class clazz = object.getClass();
        // 根据传入的属性获取Field对象，因为不确定属性的权限，用的暴力反射
        Field field = clazz.getDeclaredField(name);
        // 让jvm不检查权限
        field.setAccessible(true);
        // 为object对象里的name属性赋值
        field.set(object, values);
        
    }
    
    // 获取对象中的属性
    public static String getProrerty(Object object, String name)
            throws ReflectiveOperationException, SecurityException {
        // 获取传入对象的字节码文件
        Class clazz = object.getClass();
        // 根据传入的属性获取Field对象，因为不确定属性的权限，用的暴力反射
        Field field = clazz.getDeclaredField(name);
        // 让jvm不检查权限
        field.setAccessible(true);
        // 获取name属性的值
        Object object2 = field.get(object);
        // System.out.println(object);
        // 将值返回
        return object2.toString();
    }
    
    // 给对象中的属性赋值(通过Map的方式)，Map里key存的是属性名，value存的是要赋的值
    public static void populat(Object object, Map map) throws ReflectiveOperationException, SecurityException {
        // 获取传入对象的字节码文件
        Class clazz = object.getClass();
        // 返回此集合中的key集合
        Set keySet = map.keySet();
        // 遍历key
        for (Object object2 : keySet) {
            // 获得value值
            Object value = map.get(object2);
            try {
                // 根据传入的key(属性)获取Field对象，因为不确定属性的权限，用的暴力反射
                Field field = clazz.getDeclaredField(object2.toString());
                // 让jvm不检查权限
                field.setAccessible(true);
                // 赋值
                field.set(object, value);
            } catch (NoSuchFieldException e) {
                // 出现异常，给出友好型提示
                System.out.println("Mdzz,属性都记不住");
            }
        }
    }
}
