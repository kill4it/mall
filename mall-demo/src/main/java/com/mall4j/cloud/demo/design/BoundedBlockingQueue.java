package com.mall4j.cloud.demo.design;


import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 设计一个有限的阻塞队列
 */
public class BoundedBlockingQueue {
    int maxLength;
    LinkedList<Integer> list;
    ReentrantLock lock;
    Condition empty; //消费者获取数据时,若队列为空就阻塞
    Condition full; //生产者生产数据时,若队列已满则阻塞

    public BoundedBlockingQueue(int capacity) {
        list = new LinkedList();
        maxLength = capacity;
        lock = new ReentrantLock();
        empty = lock.newCondition();
        full = lock.newCondition();
    }

    //入队
    public void enqueue(int element) throws InterruptedException {
        lock.lock();
        try {
            while (list.size() >= maxLength) { // 修改为 >=，防止在达到最大长度时插入新元素
                full.await();
            }
            list.addLast(element);
            empty.signal();
        } finally {
            lock.unlock();
        }
    }

    //出队
    public int dequeue() throws InterruptedException {
        lock.lock();
        try {
            while (list.isEmpty()) {
                empty.await();
            }
            int res = list.removeFirst();
            full.signal();
            return res;
        } finally {
            lock.unlock();
        }
    }


    public static void main(String[] args) throws InterruptedException {

        BoundedBlockingQueue boundedBlockingQueue = new BoundedBlockingQueue(100);

        ExecutorService threadPool = Executors.newFixedThreadPool(3);
        //生产者
        threadPool.execute(() -> {
            try {
                for (int i = 0; i < 100; i++) {
                    boundedBlockingQueue.enqueue(i);
                    Thread.sleep(100);
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

        });
        threadPool.execute(() -> {
            while (true) {
                try {
                    System.out.println("消费者1 获取消息：" + boundedBlockingQueue.dequeue());
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

        });

        threadPool.execute(() -> {
            while (true) {
                try {
                    System.out.println("消费者2 获取消息：" + boundedBlockingQueue.dequeue());
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

        });
    }

}
