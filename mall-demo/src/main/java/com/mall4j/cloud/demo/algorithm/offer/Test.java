package com.mall4j.cloud.demo.algorithm.offer;

import java.util.*;

public class Test {
    public static void main(String[] args) {
        //printStar(5);
        //反转数组
//        int[] arr = {8, 9, 10, 1, 2, 3, 4, 5, 6, 7,};
//        System.out.print(minArray(arr));
/*        ListNode l1 = new ListNode(9);
        for (int i = 0; i < 20; i++) {
            l1 = new ListNode(i, l1);
        }

        ListNode l2 = new ListNode(9);
        for (int i = 0; i < 20; i++) {
            l2 = new ListNode(i, l2);
        }
        ListNode listNode = addTwoNumbers(l1, l2);
        while (listNode != null) {
            System.out.print(listNode.val + " ");
            listNode = listNode.next;
        }*/

/*
System.out.println(isIsomorphic("paper", "title"));
*/

        /*        System.out.println(wordPattern("abba", "dogs mall mall dog"));
         * */
        // System.out.println(isAnagram("anamagr", "naamgar"));

        //System.out.println(isHappy(19));
        // System.out.println(majorityElement(new int[]{1, 2, 3, 111, 1, 1, 1, 1}));

/*        int[] ints = {1, 2, 3, 111, 8, 7, 6, 5};
        System.out.println(Arrays.toString(ints));
        rotate(ints, 4);
        System.out.print(Arrays.toString(ints));*/

/*        String[] testCases = {"III", "IV", "IX", "LVIII", "MCMXCIV"};
        for (String testCase : testCases) {
            System.out.println("Roman: " + testCase + ", Integer: " + romanToInt(testCase));
        }*/

        /*        System.out.println(lengthOfLastWord("HELLO WORLD"));*/

        //  System.out.println(isSubsequence("axc", "ahbgdc"));

        List<String> set1 = Arrays.asList("计算机科学", "软件信息工程", "电子信息工程", "通信工程", "临床护理", "数据结构", "地理科学类", "城乡规划建设");
        List<String> set2 = Arrays.asList("临床护理学", "电子信息技术", "数据开发", "计算机科学技术", "通讯工程", "城乡规划工程(城乡规划建设)", "地理信息科学", "软件信息(软件工程)");

        Map<String, String> matchResult = findBestMatches(set1, set2);

        for (Map.Entry<String, String> entry : matchResult.entrySet()) {
            System.out.println(entry.getKey() + " -> " + entry.getValue());
        }

/*        System.out.println(isValid("(]"));*/

    }

    public static Map<String, String> findBestMatches(List<String> set1, List<String> set2) {
        Map<String, String> matches = new HashMap<>();

        for (String s1 : set1) {
            String bestMatch = null;
            int maxIntersectionSize = 0;
            int minDistance = Integer.MAX_VALUE;

            for (String s2 : set2) {
                // 计算字符集交集的大小
                int intersectionSize = calculateIntersectionSize(s1, s2);
                int minLength = Math.min(s1.length(), s2.length());

                // 检查交集大小是否超过阈值
                if (intersectionSize >= 0.7 * minLength) { // 阈值设为70%
                    bestMatch = s2;
                    break;
                }

                // 如果没有满足包含关系，计算 Levenshtein 距离
                int distance = levenshteinDistance(s1, s2);
                if (distance < minDistance) {
                    minDistance = distance;
                    bestMatch = s2;
                    maxIntersectionSize = intersectionSize;
                }
            }

            matches.put(s1, bestMatch);
        }

        return matches;
    }

    public static int calculateIntersectionSize(String s1, String s2) {
        Set<Character> set1 = new HashSet<>();
        Set<Character> set2 = new HashSet<>();

        for (char c : s1.toCharArray()) {
            set1.add(c);
        }

        for (char c : s2.toCharArray()) {
            set2.add(c);
        }

        set1.retainAll(set2); // 保留交集

        return set1.size();
    }

    public static int levenshteinDistance(String s1, String s2) {
        int[][] dp = new int[s1.length() + 1][s2.length() + 1];

        for (int i = 0; i <= s1.length(); i++) {
            dp[i][0] = i;
        }

        for (int j = 0; j <= s2.length(); j++) {
            dp[0][j] = j;
        }

        for (int i = 1; i <= s1.length(); i++) {
            for (int j = 1; j <= s2.length(); j++) {
                int cost = (s1.charAt(i - 1) == s2.charAt(j - 1)) ? 0 : 1;
                dp[i][j] = Math.min(Math.min(dp[i - 1][j] + 1, dp[i][j - 1] + 1), dp[i - 1][j - 1] + cost);
            }
        }

        return dp[s1.length()][s2.length()];
    }

    //打印星号
//                       *
//                      ***
//                     *****
//                    *******
//                   *********
    public static void printStar(int n) {
        int totalWidth = 21; // 总宽度，包括星号和两侧空格
        int maxStars = 10; // 星号数量的最大值

        for (int i = 1; i <= maxStars; i++) {
            if (i % 2 == 0) {
                continue;
            }
            int numSpaces = (totalWidth - i) / 2; // 计算两侧空格数量
            for (int j = 0; j < numSpaces; j++) {
                System.out.print(" "); // 打印左侧空格
            }
            for (int k = 0; k < i; k++) {
                System.out.print("*"); // 打印星号
            }
            for (int l = 0; l < numSpaces; l++) {
                System.out.print(" "); // 打印右侧空格
            }
            System.out.println(); // 换行
        }
    }

    //list转树结构
    public TreeNode1 convertTree(List<TreeNode1> nodeList) {
        Map<Integer, TreeNode1> nodeMap = new HashMap<>(); // 用于快速查找节点

        // 将原始数据转换为Map，便于通过id快速查找节点
        for (TreeNode1 node : nodeList) {
            nodeMap.put(node.getId(), node);
        }

        for (TreeNode1 node : nodeList) {
            // 查找当前节点的父节点
            int parentId = node.getParentId();
            TreeNode1 parentNode = nodeMap.get(parentId);

            if (parentNode != null) {
                // 如果找到父节点，则将当前节点添加到父节点的children列表中
                parentNode.addChild(node);
            }
        }

        // 找到根节点（即parentId为null或不存在的节点）
        for (TreeNode1 node : nodeList) {
            if (node.getParentId() == null || !nodeMap.containsKey(node.getParentId())) {
                return node;
            }
        }

        return null; // 如果没有找到根节点，返回null（这种情况应避免，通常意味着数据有误）
    }

    public static class TreeNode1 {
        private final Integer id;
        private final Integer parentId;
        private List<TreeNode1> children;

        public TreeNode1(Integer id, Integer parentId) {
            this.id = id;
            this.parentId = parentId;
        }

        public Integer getId() {
            return id;
        }

        public Integer getParentId() {
            return parentId;
        }

        // 构造函数、getter/setter 方法以及其他必要属性省略...

        public void addChild(TreeNode1 child) {
            if (children == null) {
                children = new ArrayList<>();
            }
            children.add(child);
        }

    }


    //剑指offer 03. 数组中重复的数字
    //找到数组中任意重复的数字 双指针 哈希表 下标法
    public int findRepeatNumber(int[] nums) {
        int[] arr = new int[nums.length];

        for (int i = 0; i < nums.length; i++) {
            if (arr[nums[i]] == 1) {
                return nums[i];
            } else {
                arr[nums[i]] = 1;
            }
        }
        //没有找到返回 -1
        return -1;

/*        int[] arr = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            if (arr[nums[i]] == 1) {
                return nums[i];
            } else {
                arr[nums[i]] = 1;
            }
        }
        return -1;*/
    }


    //剑指offer 04. 二维数组中的查找
    //从左到右递增，从上到下递增的二维数组
    public boolean findNumberIn2DArray(int[][] matrix, int target) {
        //暴力解法
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] == target) {
                    return true;
                }
            }
        }


        //借助二分法,从左下角开始遍历,借助右侧数据比左侧大,顶部数据比底部小的规律
        if (matrix.length == 0 || matrix[0].length == 0) {
            return false;
        }
        int rows = matrix.length;
        int cols = matrix[0].length;

        int row = rows - 1;
        int col = 0;

        while (row >= 0 && col < cols) {//结束遍历的条件（目标数超出边界）
            int value = matrix[row][col];
            if (value == target) {
                return true;
            }
            if (target < value) {
                row--; //target 比目标数小,一定在顶部,向上移动
            }
            if (target > value) {
                col++; //target 比目标数大,一定在右侧,向右移动
            }
        }
        return false;

    }

    //剑指offer 05. 替换空格,空格替换为%20
    public String replaceSpace(String s) {
        //动态数组方式
        int originalLength = s.length();
        // Step 1: Count the number of spaces in the input string.
        int spaceCount = 0;
        for (char c : s.toCharArray()) {
            if (c == ' ') {
                spaceCount++;
            }
        }

        // Step 2: Calculate the new length after replacing spaces with "%20" (3 characters each).
        int newLength = originalLength + spaceCount * 2;

        // Step 3: Create a dynamic character array with the new length.
        char[] resultArray = new char[newLength];

        // Step 4: Copy characters from the input string to the result array, replacing spaces with "%20".
        int index = 0, spaceIndex = 0;
        for (char c : s.toCharArray()) {
            if (c == ' ') {
                resultArray[index++] = '%';
                resultArray[index++] = '2';
                resultArray[index++] = '0';
                spaceIndex += 2; // Increment the space index by 2 since we added 2 extra characters
            } else {
                resultArray[index++] = c;
            }
        }

        // Step 5: Convert the character array to a string and return it.
        return new String(resultArray);



/*      StringBuilder方式
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ') {
                sb.append("%20");
            } else {
                sb.append(s.charAt(i));
            }
        }
        return sb.toString();*/
    }

    //剑指offer 06. 从尾到头打印链表
    public int[] reversePrint(ListNode head) {
        int count = 0; //链表长度
        ListNode node = head;
        while (node != null) {
            count++;
            node = node.next;
        }
        int[] arr = new int[count];
        int index = count - 1;
        while (head != null) {
            arr[index--] = head.val;
            head = head.next;
        }
        return arr;
    }

    private static class ListNode {
        int val;
        ListNode next;

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        public void setNext(ListNode next) {
            this.next = next;
        }

        public ListNode getNext(ListNode next) {
            return this.next;
        }

        public void setVal(int val) {
            this.val = val;
        }

        public int getVal(int val) {
            return val;
        }
    }

    //TODO 剑指offer 07. 重建二叉树 根据前序遍历和中序遍历的结果重建二叉树
    public TreeNode buildTree(int[] preorder, int[] inorder) {

        return null;
    }

    private static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    //剑指offer 09. 用两个栈实现队列
    public class QueueWithTwoStacks {
        private final Stack<Integer> stack1; // 用于添加元素
        private final Stack<Integer> stack2; // 用于弹出元素

        public QueueWithTwoStacks() {
            stack1 = new Stack<>();
            stack2 = new Stack<>();
        }

        // 入队操作：直接将元素压入 stack1
        public void enqueue(int value) {
            stack1.push(value);
        }

        // 出队操作：
        // 如果 stack2 为空，则将 stack1 的所有元素依次弹出并压入 stack2，此时 stack2 顶部元素即为队首元素
        // 然后弹出 stack2 的顶部元素，即完成出队
        public int dequeue() {
            if (stack2.isEmpty()) {
                while (!stack1.isEmpty()) {
                    stack2.push(stack1.pop());
                }
            }

            if (!stack2.isEmpty()) {
                return stack2.pop();
            } else {
                throw new IllegalStateException("Queue is empty");
            }
        }
    }

    //剑指offer 10. 斐波那契数列
    public void printFibonacciSequence(int n) {
        if (n > 0) {
            printFibonacciSequence(n - 1); // 先打印前面的项
            System.out.print(fibonacci(n) + " "); // 再打印当前项
        }
    }

    public int fibonacci(int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    //剑指offer 11. 青蛙跳台阶
    public int numWays(int n) {
        //递归方法
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        if (n == 2) {
            return 2;
        }
        return numWays(n - 1) + numWays(n - 2);

        /* 动态规划
    if (n <= 2) {
        return n;
    }

    int dp1 = 1; // 跳1级台阶的跳法数
    int dp2 = 2; // 跳2级台阶的跳法数

    // 从第3级台阶开始，动态计算跳法数
    for (int i = 3; i <= n; i++) {
        int dp3 = dp1 + dp2; // 当前台阶的跳法数等于前两级台阶跳法数之和
        dp1 = dp2; // 更新dp1为前一级台阶的跳法数
        dp2 = dp3; // 更新dp2为当前台阶的跳法数
    }

    return dp2; // 返回最后计算得到的跳上n级台阶的跳法数*/

    }

    //剑指offer 11. 旋转数组中的最小值
    public static int minArray(int[] nums) {

/*        //暴力解法
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] < nums[i - 1]) {
                return nums[i];
            }
        }
        return nums[0];  // 若数组未发生旋转，最小值为首个元素
        */


        //二分查找
        int left = 0, right = nums.length - 1;

        while (left < right) {
            int mid = left + (right - left) / 2;

            if (nums[mid] < nums[right]) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }

        return nums[left];

    }

    //二分查找
    public static int search(int[] nums, int target) {
        int left = 0, right = nums.length - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] == target) {
                return mid;
            } else if (nums[mid] < target) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return -1;
    }

    //TODO 剑指offer 12. 矩阵中的路径
    public boolean exist(char[][] board, String word) {
        return false;
    }

    //TODO 剑指offer13. 机器人的运动范围
    public int movingCount(int m, int n, int k) {
        return 0;
    }

    // 剑指offer 14. 剪绳子
    public int cuttingRope(int bambooLen) {
        // 初始化动态规划数组
        int[] dp = new int[bambooLen + 1];
        // 基本情况：长度为 1 的竹子只能得到一个长度为 1 的段，所以其乘积为 1
        dp[1] = 1;
        // 动态规划递推
        for (int i = 2; i <= bambooLen; i++) {
            // 试着从长度为 1 到 i-1 的每个位置切一刀，找到能产生最大乘积的组合
            int maxProductSoFar = 0;
            for (int j = 1; j < i; j++) {
                maxProductSoFar = Math.max(maxProductSoFar, dp[j] * dp[i - j]);
            }
            dp[i] = maxProductSoFar;
        }
        // 最终结果位于 dp 数组的最后一项
        return dp[bambooLen];
    }

    //剑指offer 15. 二进制中1的个数
    public int hammingWeight(int n) {
        int count = 0;
        while (n != 0) {
            int bit = n & 1;//如果两个位都是1；则结果位是 1 否则为 0
            // 计算最低位的1
            count = count + bit;
            // 把n向右移动一位
            n >>= 1;
        }
        return count;
    }

    //剑指offer 16. 数值的整数次方
    public double myPow(double x, int n) {
        //递归方法
        if (n == 0) {
            return 1;
        }
        if (n == 1) {
            return x;
        }
        if (n == -1) {
            return 1 / x;
        }
        if (n % 2 == 0) {
            //偶次幂
            return myPow(x * x, n / 2);
        } else {
            //奇次幂
            return x * myPow(x * x, n / 2);
        }
    }

    //剑指offer 17. 打印从1到最大的n位数
    public static int[] printNumbers(int n) {
        int start = 1;
        int end = (int) Math.pow(10, n) - 1; // 计算最大的n位数
        int[] nums = new int[end + 1];
        for (int i = start; i <= end; i++) {
            nums[i - 1] = i;
        }
        return nums;
    }

    //剑指offer 18. 删除链表的节点
    public ListNode deleteNode(ListNode head, int val) {
        if (head == null) {
            return null;
        }
        if (head.val == val) {
            return head.next;
        }


        ListNode cur = head;
        while (cur.next != null) {
            if (cur.next.val == val) {
                cur.next = cur.next.next;
                break;
            }
            cur = cur.next;
        }
        return head;
    }

    //TODO 剑指offer 19. 正则表达式匹配
    public boolean isMatch(String s, String p) {
        return false;
    }

    //剑指offer 20. 表示数值的字符串
    public boolean isNumeric(String s) {
        //主要考察各种情况的考虑 //各种情况  "+100" "5e2" "-123.1" "3.14626" "-1E-16" "0123"
        //2 .小数点的情况 3.E/e 4.正负号 5.0开头 6.0结尾 7.0.0
        // 去除字符串两端的空格
        String trimmed = s.trim();

        if (trimmed.isEmpty()) {
            return false;
        }

        int start = 0;
        int end = trimmed.length() - 1;

        // 处理可能存在的正负号
        char firstChar = trimmed.charAt(start);
        if (firstChar == '+' || firstChar == '-') {
            start++;
        }

        // 检查是否仅有一个点
        if (trimmed.charAt(start) == '.' || (end > start && trimmed.charAt(end) == '.')) {
            return false;
        }

        // 查找可能存在的小数点和'e/E'的位置
        int dotIndex = -1;
        int expIndex = -1;
        for (int i = start; i <= end; i++) {
            char c = trimmed.charAt(i);
            if (c == '.') {
                if (dotIndex != -1) {
                    return false; // 多个小数点
                }
                dotIndex = i;
            } else if (c == 'e' || c == 'E') {
                if (expIndex != -1) {
                    return false; // 多个'e/E'
                }
                expIndex = i;
            } else if (!Character.isDigit(c)) {
                return false; // 非数字、点、'e/E'字符
            }
        }

        // 如果有'e/E'，确保其后紧跟整数
        if (expIndex != -1) {
            if (expIndex == end || expIndex == start) {
                return false; // 'e/E'位于字符串开头或末尾，没有跟随整数
            }
            for (int i = expIndex + 1; i <= end; i++) {
                if (!Character.isDigit(trimmed.charAt(i))) {
                    return false; // 'e/E'后有非数字字符
                }
            }
        }

        // 此时字符串已经通过所有检查，表示它是一个有效的数值
        return true;
    }

    //剑指offer 21. 调整数组顺序使奇数位于偶数前面
    public int[] exchange(int[] nums) {
        //双指针 遍历数组，将奇数交换到偶数前面
        int left = 0; // 奇数指针，从数组左侧开始
        int right = nums.length - 1; // 偶数指针，从数组右侧开始
        while (left < right) {
            // 找到奇数 左指针右移 直到找到左侧的偶数
            while (left < right && (nums[left] % 2) == 1) {
                left++;
            }
            // 找到偶数 右指针左移 直到找到右侧的奇数
            while (left < right && (nums[right] % 2) == 0) {
                right--;
            }
            if (left >= right) {
                break;
            }
            // 交换奇数和偶数
            int temp = nums[left];
            nums[left] = nums[right];
            nums[right] = temp;
        }
        return nums;
    }

    //剑指offer 22. 链表中倒数第k个节点
    public ListNode getKthFromEnd(ListNode head, int k) {
        ListNode fast = head;
        ListNode slow = head;
        //快指针先走k步
        for (int i = 0; i < k; i++) {
            fast = fast.next;
        }
        //快慢指针一起走，当快指针到达末尾时，慢指针指向倒数第k个节点
        while (fast != null) {
            fast = fast.next;
            slow = slow.next;
        }
        return slow;
    }

    //剑指offer 23. 链表中环的入口节点
    public ListNode detectCycle(ListNode head) {
        //1.双指针法  2.动态规划
        if (head == null || head.next == null) {
            return null; // 链表为空或只有一个节点，无环
        }

        ListNode slow = head;
        ListNode fast = head;

        // 快慢指针初始化在同一位置，快指针每次移动两步，慢指针每次移动一步
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;

            // 若快慢指针相遇，说明存在环
            if (slow == fast) {
                break;
            }
        }

        // 没有相遇则无环，返回null
        if (fast == null || fast.next == null) {
            return null;
        }

        // 重置快指针到头节点，慢指针保持在相遇点
        fast = head;
        while (fast != slow) {
            fast = fast.next;
            slow = slow.next;
        }

        // 相遇时，fast指针指向的就是环的入口节点
        return fast;
    }

    //剑指offer 24. 反转链表
    public ListNode reverseList(ListNode head) {
        ListNode prev = null; // 前驱节点指针
        ListNode curr = head; // 当前节点指针

        while (curr != null) {
            ListNode nextTemp = curr.next; // 保存当前节点的下一个节点
            curr.next = prev; // 反转当前节点的指针，使其指向前驱节点
            prev = curr; // 将当前节点作为新的前驱节点
            curr = nextTemp; // 移动当前节点到下一个节点
        }

        return prev; // 反转后的链表头节点为原链表的尾节点
    }

    //剑指offer 25. 合并两个排序的链表
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode dummyHead = new ListNode(0); // 创建一个虚拟头节点，便于后续操作
        ListNode tail = dummyHead; // 初始化尾指针，始终指向已合并链表的尾部

        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                tail.next = l1;
                l1 = l1.next;
            } else {
                tail.next = l2;
                l2 = l2.next;
            }
            tail = tail.next; // 更新尾指针
        }

        // 将剩余非空链表接到已合并链表的尾部
        if (l1 != null) {
            tail.next = l1;
        } else {
            tail.next = l2;
        }

        return dummyHead.next; // 返回合并后链表的真正头节点（即虚拟头节点的下一个节点）
    }

    //剑指offer 26. 树的子结构
    public boolean isSubStructure(TreeNode A, TreeNode B) {
        if (A == null || B == null) {
            return false;
        }
        if (isSubTree(A, B)) {
            return true;
        }
        return isSubStructure(A.left, B) || isSubStructure(A.right, B);
    }

    private boolean isSubTree(TreeNode Ta, TreeNode Tb) {
        if (Tb == null) {
            return true;
        }
        if (Ta == null) {
            return false;
        }
        if (Ta.val != Tb.val) {
            return false;
        }
        return isSubTree(Ta.left, Tb.left) && isSubTree(Ta.right, Tb.right);
    }

    //剑指offer 26. 二叉树的镜像
    public TreeNode mirrorTree(TreeNode root) {
        if (root == null) {
            return null;
        }
        // 交换当前节点的左右子树
        TreeNode temp = root.left;
        root.left = root.right;
        root.right = temp;

        // 递归地对左右子树进行镜像操作
        mirrorTree(root.left);
        mirrorTree(root.right);

        return root;
    }

    //剑指offer 27. 对称的二叉树
    public boolean isSymmetric(TreeNode root) {
        if (root == null || (root.left == null && root.right == null)) {
            return true;
        }
        return f(root.left, root.right);
    }

    private boolean f(TreeNode A, TreeNode B) {
        if (A == null && B == null) {
            return true;
        } else if (A == null || B == null) {
            return false;
        } else if (A.val != B.val) {
            return false;
        }
        return f(A.left, B.right) && f(A.right, B.left);
    }

    //剑指offer 29. 顺时针打印矩阵
    public int[] spiralOrder(int[][] matrix) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return new int[0];
        }

        int rows = matrix.length;
        int cols = matrix[0].length;
        int[] result = new int[rows * cols];
        int index = 0;

        int left = 0, right = cols - 1, top = 0, bottom = rows - 1;

        while (left <= right && top <= bottom) {
            // 从左到右打印顶部一行
            for (int i = left; i <= right; i++) {
                result[index++] = matrix[top][i];
            }
            // 从上到下打印右侧一列，注意下标要更新
            for (int i = top + 1; i <= bottom; i++) {
                result[index++] = matrix[i][right];
            }
            // 如果还有下一行（即还未打印到底部），继续打印
            if (top < bottom) {
                for (int i = right - 1; i >= left; i--) {
                    result[index++] = matrix[bottom][i];
                }
            }
            // 如果还有下一列（即还未打印到最左侧），继续打印
            if (left < right) {
                for (int i = bottom - 1; i > top; i--) {
                    result[index++] = matrix[i][left];
                }
            }
            // 缩小边界，准备进入下一层螺旋
            left++;
            right--;
            top++;
            bottom--;
        }

        return result;
    }


    //两数之和
    //给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
    //你可以假设每种输入只会对应一个答案。但是，你不能重复利用这个数组中同样的元素。
    public int[] twoSum(int[] nums, int target) {
        // 创建一个哈希表来存储已经遍历过的数字及其索引
        Map<Integer, Integer> numMap = new HashMap<>();
        // 遍历数组
        for (int i = 0; i < nums.length; i++) {
            // 计算补数
            int complement = target - nums[i];
            // 检查补数是否在哈希表中
            if (numMap.containsKey(complement)) {
                // 如果在，返回结果
                return new int[]{numMap.get(complement), i};
            }
            // 如果不在，将当前数字及其索引存入哈希表
            numMap.put(nums[i], i);
        }
        // 如果没有找到，抛出异常（根据题目假设，这种情况不会发生）
        throw new IllegalArgumentException("No two sum solution");
    }


    //两数相加
    // Definition for singly-linked list.
    //输入：l1 = [2,4,3], l2 = [5,6,4]
    //输出：[7,0,8]
    //解释：342 + 465 = 708.

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        // 创建虚拟头节点
        ListNode dummy = new ListNode(0);
        ListNode current = dummy;
        int carry = 0;
        while (l1 != null || l2 != null || carry != 0) {
            //获取到相加的数 当前值 = l1的值 + l2的值 + 进位
            int sum = (l1 != null ? l1.val : 0) + (l2 != null ? l2.val : 0) + carry;
            //获取进位 = 相加的数 / 10
            carry = sum / 10;
            //设置当前节点的值 = 相加的数 % 10
            current.next = new ListNode(sum % 10);
            //移动指针
            current = current.next;
            if (l1 != null) l1 = l1.next;
            if (l2 != null) l2 = l2.next;

        }
        return dummy.next;
    }

    //最长无重复字符的子串
    //给定一个字符串，请你找出其中不含有重复字符的 最长子串 的长度。
    public int lengthOfLongestSubstring(String s) {
        HashMap<Character, Integer> map = new HashMap<>();
        int left = 0;
        int right = 0;
        int maxLength = 0;
        while (right < s.length()) {
            char c = s.charAt(right);
            if (map.containsKey(c)) {
                //字符 c 已经在窗口中出现过时，将左边界 left 移动到 c 上次出现位置的下一个位置
                left = Math.max(map.get(c) + 1, left);
            }
            //字符不存在,更新最长的长度
            maxLength = Math.max(maxLength, right - left + 1);
            //记录当前字符串出现的最新位置
            map.put(c, right);
            //右边界向右移动
            right++;
        }
        return maxLength;

    }

    //双指针 leetcode 125.验证回文串
    public boolean isPalindrome(String s) {
        if (s == null || s.isEmpty()) {
            return false;
        }
        int left = 0;
        int right = s.length() - 1;
        while (left < right) {
            //找到左侧的字母或数字字符
            while (left < right && !Character.isLetterOrDigit(s.charAt(left))) {
                left++;
            }
            //找到右侧的字母或数字字符
            while (left < right && !Character.isLetterOrDigit(s.charAt(right))) {
                right--;
            }
            // 比较字符，忽略大小写
            if (Character.toLowerCase(s.charAt(left)) != Character.toLowerCase(s.charAt(right))) {
                return false;
            }

            left++;
            right--;
        }


        return true;
    }

    //leetcode.88 合并两个有序数组
    public int[] merge(int[] nums1, int m, int[] nums2, int n) {
        int[] result = new int[m + n];
        int i = 0, j = 0, k = 0;
        while (i < m && j < n) {
            if (nums1[i] < nums2[j]) {
                result[k] = nums1[i];
                i++;
            } else {
                result[k] = nums2[j];
                j++;
            }
            k++;
        }
        while (i < m) {
            result[k] = nums1[i];
            i++;
            k++;
        }
        while (j < n) {
            result[k] = nums2[j];
            k++;
            j++;
        }
        return result;
    }

    //leetcode.383 赎金信 使用hashmap解决
    public boolean canConstruct(String ransomNote, String magazine) {
        //使用hashmap解决
        Map<Character, Integer> hashmap = new HashMap<>();
        for (int i = 0; i < magazine.length(); i++) {
            //value记录字符次数
            hashmap.put(magazine.charAt(i), hashmap.getOrDefault(magazine.charAt(i), 0) + 1);
        }
        //判断
        for (int i = 0; i < ransomNote.length(); i++) {
            if (hashmap.containsKey(ransomNote.charAt(i)) && hashmap.get(ransomNote.charAt(i)) > 0) {
                //字符次数减一
                hashmap.put(ransomNote.charAt(i), hashmap.get(ransomNote.charAt(i)) - 1);
            } else {
                return false;
            }
        }
        return true;

    }

    //leetcode .205 同构字符串
    public static boolean isIsomorphic(String s, String t) {
        HashMap<Character, Character> sMap = new HashMap<>();
        HashMap<Character, Character> tMap = new HashMap<>();

        for (int i = 0; i < s.length(); i++) {

            char sc = s.charAt(i);
            char tc = t.charAt(i);
            if (sMap.containsKey(sc)) {
                Character stc = sMap.get(sc);
                if (!stc.equals(tc)) {
                    return false;
                }
            } else {
                sMap.put(sc, tc);

            }

            if (tMap.containsKey(tc)) {
                Character tsc = tMap.get(tc);
                if (!tsc.equals(sc)) {
                    return false;
                }
            } else {
                tMap.put(tc, sc);
            }

        }

        return true;

    }

    //leetcode .290 单词规律
    public static boolean wordPattern(String pattern, String s) {
        //" "分隔s出s中的每个单词
        String[] words = s.split(" ");
        //长度不匹配
        if (words.length != pattern.length()) {
            return false;
        }

        //创建两个hashmap,保存互相的对应关系
        Map<Character, String> patternMap = new HashMap<>();
        Map<String, Character> wordMap = new HashMap<>();

        for (int i = 0; i < pattern.length(); i++) {
            char c = pattern.charAt(i);
            String word = words[i];

            String cWord = patternMap.get(c);
            if (cWord != null && !cWord.equals(word)) {
                return false;
            } else {
                patternMap.put(c, word);
            }
            Character wChar = wordMap.get(word);
            if (wChar != null && !wChar.equals(c)) {
                return false;
            } else {
                wordMap.put(word, c);
            }

        }
        return true;
    }

    //leetcode .242 有效的字母异位词
    public static boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        //保存字符出现的次数
        Map<Character, Integer> hashMap = new HashMap<>();

        for (int i = 0; i < s.length(); i++) {
            char sc = s.charAt(i);
            Integer count = hashMap.get(sc);
            if (count == null) {
                hashMap.put(sc, 1);
            } else {
                hashMap.put(sc, count + 1);
            }
        }

        for (int i = 0; i < t.length(); i++) {
            char tc = t.charAt(i);
            Integer count = hashMap.get(tc);
            if (count == null || count == 0) {
                return false;
            } else {
                hashMap.put(tc, count - 1);
            }
        }

        return true;
    }

    //leetcode 202.快乐数
    public static boolean isHappy(int n) {
        //如果出现了循环则肯定不是快乐数
        Set<Integer> set = new HashSet<>();
        while (n != 1 && !set.contains(n)) {
            set.add(n);
            n = getNext(n); //获取下一个数字
        }
        return n == 1;
    }

    private static int getNext(int n) {
        //求每个位数的平法
        int sum = 0;
        while (n > 0) {
            int d = n % 10;
            sum = sum + d * d;
            n = n / 10;
        }
        return sum;
    }

    //leetcode 219.存在重复元素2
    public static boolean containsNearbyDuplicate(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            Integer j = map.get(num);
            if (j != null && Math.abs(i - j) <= k) {
                return true;
            }
            map.put(num, i);
        }
        return false;
    }

    //leetcode 67. 二进制求和
    public String addBinary(String a, String b) {
        StringBuilder result = new StringBuilder();
        int i = a.length() - 1;
        int j = b.length() - 1;
        int carry = 0;

        while (i >= 0 || j >= 0 || carry > 0) {
            int sum = carry;
            if (i >= 0) {
                sum = sum + a.charAt(i) - '0';
                i--;
            }
            if (j >= 0) {
                sum = sum + b.charAt(j) - '0';
                j--;
            }
            //计算进位
            carry = sum / 2;
            //计算当前位数
            result.append(sum % 2);
        }

        return result.reverse().toString();

    }

    //leetcode 169. 多数元素
    public static int majorityElement(int[] nums) {
        int n = nums.length / 2;

        Map<Integer, Integer> map = new HashMap<>();

        for (int num : nums) {
            Integer i = map.get(num);
            if (i == null) {
                map.put(num, 1);
            } else {
                if (i >= n) {
                    return num;
                } else {
                    map.put(num, i + 1);
                }
            }
        }

        return -1;

    }

    //leetcode 189. 轮转数组
    public static void rotate(int[] nums, int k) {
/*      自己的做法
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < k; i++) {
            int num = nums[nums.length - i-1];
            stack.push(num);
        }

        int[] rot = new int[nums.length];
        for (int i = 0; i < k; i++) {
            rot[i] = stack.pop();
        }
        for (int i = 0; i < nums.length - k; i++) {
            rot[k+i] = nums[i];
        }
        nums = rot;
        System.out.println(Arrays.toString(nums));*/
        if (nums == null || nums.length == 0) return;

        k = k % nums.length; // 处理 k 大于数组长度的情况
        if (k == 0) return;

        reverse(nums, 0, nums.length - 1); // 全部反转
        reverse(nums, 0, k - 1); // 前 k 个元素反转
        reverse(nums, k, nums.length - 1); // 剩余元素反转
    }

    private static void reverse(int[] nums, int start, int end) {
        while (start < end) {
            int temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start++;
            end--;
        }
    }

    //leetcode 121. 买卖股票的最佳时机
    public int maxProfit(int[] prices) {
/*      自己的做法
        int max = 0;
        int lowindex = 0;
        int highindex = 0;

        for (int i = 0; i < prices.length; i++) {
            int low = prices[i];
            for (int j = i + 1; j < prices.length; j++) {
                int high = prices[j];
                if (high - low > max) {
                    max = high - low;
                    lowindex = i;
                    highindex = j;
                }
            }
        }
        return max;*/

        if (prices == null || prices.length == 0) {
            return 0;
        }

        int minPrice = Integer.MAX_VALUE; // 记录最低价格
        int maxProfit = 0; // 记录最大利润

        for (int price : prices) {
            if (price < minPrice) {
                minPrice = price; // 更新最低价格
            } else if (price - minPrice > maxProfit) {
                maxProfit = price - minPrice; // 更新最大利润
            }
        }

        return maxProfit;
    }


    //leetcode 122.买股票的最佳时机2
    public static int maxProfit2(int[] prices) {
        if (prices == null || prices.length <= 1) {
            return 0;
        }

        int maxProfit = 0;

        for (int i = 1; i < prices.length; i++) {
            if (prices[i] > prices[i - 1]) {
                maxProfit += prices[i] - prices[i - 1]; // 累加所有上升区间的利润
            }
        }

        return maxProfit;
    }

    //leetcode 13.罗马数字转整数
    public static int romanToInt(String s) {

        if (s == null || s.isEmpty()) {
            throw new IllegalArgumentException("Input string cannot be null or empty");
        }

        HashMap<Character, Integer> romanMap = new HashMap<>();
        romanMap.put('I', 1);
        romanMap.put('V', 5);
        romanMap.put('X', 10);
        romanMap.put('L', 50);
        romanMap.put('C', 100);
        romanMap.put('D', 500);
        romanMap.put('M', 1000);

        int res = 0;

        for (int i = 0; i < s.length(); i++) {
            Integer currentNum = romanMap.get(s.charAt(i));
            if (i > 0 && romanMap.get(s.charAt(i - 1)) < currentNum) {
                res += currentNum - romanMap.get(s.charAt(i - 1)) - romanMap.get(s.charAt(i - 1));
            } else {
                res += currentNum;
            }
        }

        return res;

    }

    //leetcode 58.最后一个单词的长度
    public static int lengthOfLastWord(String s) {
/*        String[] split = s.split(" ");
        return split[split.length - 1].length();
        */

        int num = 0;
        int lastindex = 0;
        for (int i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) != ' ') {
                lastindex = i;
                break;
            }
        }
        for (int i = lastindex; i >= 0; i--) {
            if (s.charAt(i) != ' ') {
                num++;
            } else {
                break;
            }
        }
        return num;
    }

    //leetcode 14.最长公共前缀
    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }
        HashMap<Integer, Character> map = new HashMap<>();
        int minLength = strs[0].length();
        //已第一个字符为基准
        for (int i = 0; i < strs[0].length(); i++) {
            map.put(i, strs[0].charAt(i));
        }
        for (int i = 1; i < strs.length; i++) {
            int minLength2 = 0;
            for (int j = 0; j < strs[i].length(); j++) {
                Character c = map.get(j);
                if (c != null && c.equals(strs[i].charAt(j))) {
                    minLength2++;
                } else {
                    break;
                }
            }
            if (minLength2 < minLength) {
                minLength = minLength2;
            }
        }
        return strs[0].substring(0, minLength);
    }

    //leetcode 28.找出字符串中第一个匹配项的下标
    public int strStr(String haystack, String needle) {
        if (haystack == null || needle == null || needle.length() > haystack.length()) {
            return -1;
        }
        int index = -1;
        for (int i = 0; i < haystack.length(); i++) {
            if (haystack.charAt(i) != needle.charAt(0)) {
                continue;
            }
            //开始匹配
            int j = i;
            for (int k = 0; k < needle.length(); k++) {
                if (j > haystack.length() - 1 || haystack.charAt(j) != needle.charAt(k)) {
                    break;
                } else {
                    j++;
                }
            }
            if (j - i == needle.length()) {
                index = i;
                break;
            }
        }
        return index;
    }


    //leetcode 382.判断子序列
    //"aaaaaa" "bbaaaa"
    public static boolean isSubsequence(String s, String t) {
/*      自己的做法

        if (s == null || t == null || s.length() > t.length()) {
            return false;
        }

        int nextIndex = 0;

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            boolean found = false;
            for (int j = nextIndex; j < t.length(); j++) {
                if (t.charAt(j) == c) {
                    nextIndex = j + 1;
                    found = true;
                    break;
                }
            }

            if (!found) {
                return false;
            }
        }
        return true;*/

        //双指针
        if (s == null || t == null) {
            return false;
        }
        //s小串  t大串
        int i = 0, j = 0;
        while (i < s.length() && j < t.length()) {
            if (s.charAt(i) == t.charAt(j)) {
                i++;
                j++;
            } else {
                j++;
            }
        }
        return i == s.length();
    }

    //leetcode 20. 有效的括号
    public static boolean isValid(String s) {
        if (s == null || s.isEmpty()) {
            return false;
        }

        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '(' || c == '{' || c == '[') {
                stack.push(c);
            } else {
                if (stack.isEmpty()){
                    return false;
                }
                char pop = stack.pop();
                if (pop == '(' && c != ')') {
                    return false;
                }
                if (pop == '[' && c != ']') {
                    return false;
                }
                if (pop == '{' && c != '}') {
                    return false;
                }
            }
        }
        if (!stack.isEmpty()){
            return false;
        }
        return true;
    }
}
