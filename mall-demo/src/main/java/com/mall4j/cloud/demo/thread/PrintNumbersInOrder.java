package com.mall4j.cloud.demo.thread;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Desc 要求用三个线程，线程1先打印1,2,3,4,5, 然后是线程2打印6,7,8,9,10, 然后是线程3打印11,12,13,14,15. 接着再由线程1打印16,17,18,19,20....以此类推，直到输出到45
 */
public class PrintNumbersInOrder {

    public static void main(String[] args) {
        Object lockObj = new Object();
        ExecutorService threadPool = Executors.newFixedThreadPool(3);

        threadPool.execute(new MyRunnable(0, lockObj));
        threadPool.execute(new MyRunnable(1, lockObj));
        threadPool.execute(new MyRunnable(2, lockObj));
        threadPool.shutdown();
    }

}


class MyRunnable implements Runnable {

    private static int num = 1;

    private final int threadId;

    private final Object lockObj;

    public MyRunnable(int threadId, Object lockObj) {
        this.threadId = threadId;
        this.lockObj = lockObj;
    }

    @Override
    public void run() {
        synchronized (lockObj) {
            while (num <= 45) {
                // num / 5 计算出这是第几次遍历打印  % 3计算出需要哪个线程id打印
                if (num / 5 % 3 == threadId) {
                    for (int i = 0; i < 5; i++) {
                        System.out.println("ThreadName: " + Thread.currentThread().getName() + ", num is: " + num++);
                    }
                    lockObj.notifyAll();
                } else {
                    try {
                        lockObj.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
