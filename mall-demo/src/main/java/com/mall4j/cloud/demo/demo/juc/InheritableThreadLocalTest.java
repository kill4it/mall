package com.mall4j.cloud.demo.demo.juc;

/**
 * 父子线程减共享ThreadLocal
 *
 * @Auther LPJ
 * @Description
 * @Data 2022/9/8 17:23
 * @Version 1.0.0
 **/
public class InheritableThreadLocalTest {
    public static void main(String[] args) {
        ThreadLocal<String> threadLocal = new ThreadLocal<>();
        InheritableThreadLocal<String> inheritableThreadLocal = new InheritableThreadLocal<>();
    
        threadLocal.set("关注公众号：捡田螺的小男孩");
        inheritableThreadLocal.set("关注公众号：程序员田螺");
    
        //在子线程init()时会把父线程的InheritableThreadLocal拷贝一份
        Thread thread = new Thread(()->{
            System.out.println("ThreadLocal value " + threadLocal.get());
            System.out.println("InheritableThreadLocal value " + inheritableThreadLocal.get());
        });
        thread.start();
    
    }
    
}
