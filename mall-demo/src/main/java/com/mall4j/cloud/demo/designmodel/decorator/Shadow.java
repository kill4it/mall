package com.mall4j.cloud.demo.designmodel.decorator;

/**
 * @Auther LPJ
 * @Description
 * @Data 2023/2/8 18:14
 * @Version 1.0.0
 **/
public class Shadow extends ShapeDecorator {
    
    public Shadow(Shape shape) {
        super(shape);
    }
    
    @Override
    public void draw() {
        super.draw();
        System.out.print(" 有阴影");
    }
}

