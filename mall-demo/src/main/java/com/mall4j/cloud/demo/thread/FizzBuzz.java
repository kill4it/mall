package com.mall4j.cloud.demo.thread;

import java.util.concurrent.Semaphore;
import java.util.function.IntConsumer;
//线程交替打印fizz buzz

/**
 * 一个线程调度其他线程的思想
 */
public class FizzBuzz {
    private final int n;

    private final int currentNum = 0;

    private volatile int state = 0;

    //countdownlatch
    //锁
    public FizzBuzz(int n) {
        this.n = n;
    }
/*    //信号量
    private final Semaphore fizzSemaphore = new Semaphore(0);
    private final Semaphore buzzSemaphore = new Semaphore(0);
    private final Semaphore fizzbuzzSemaphore = new Semaphore(0);
    private final Semaphore numberSemaphore = new Semaphore(1);
    // printFizz.run() outputs "fizz".
    public void fizz(Runnable printFizz) throws InterruptedException {
        //3
        while (currentNum <= n) {
            fizzSemaphore.acquire();
            printFizz.run();
            numberSemaphore.release();
        }
    }

    // printBuzz.run() outputs "buzz".
    public void buzz(Runnable printBuzz) throws InterruptedException {
        //5
        while (currentNum <= n) {
            buzzSemaphore.acquire();
            printBuzz.run();
            numberSemaphore.release();
        }
    }

    // printFizzBuzz.run() outputs "fizzbuzz".
    public void fizzbuzz(Runnable printFizzBuzz) throws InterruptedException {
        // 3 5
        while (currentNum <= n) {
            fizzbuzzSemaphore.acquire();
            printFizzBuzz.run();
            numberSemaphore.release();
        }
    }

    // printNumber.accept(x) outputs "x", where x is an integer.
    public void number(IntConsumer printNumber) throws InterruptedException {
        while (currentNum <= n) {
            numberSemaphore.acquire();
            currentNum++;
            if (currentNum % 3 == 0 && currentNum % 5 == 0) {
                fizzbuzzSemaphore.release();
            } else if (currentNum % 3 == 0) {
                fizzSemaphore.release();
            } else if (currentNum % 5 == 0) {
                buzzSemaphore.release();
            }else {
                printNumber.accept(currentNum);
                numberSemaphore.release();
            }

        }


    }*/

    //无锁 （自旋的思想）
 /*   private void fizz(Runnable fizz) throws InterruptedException {
        while (currentNum <= n) {
            if (currentNum % 3 == 0 && currentNum % 5 != 0) {
                fizz.run();
                currentNum++;
            } else {
                Thread.yield();
            }
        }
    }

    private void buzz(Runnable fizz) throws InterruptedException {
        while (currentNum <= n) {
            if (currentNum % 3 != 0 && currentNum % 5 == 0) {
                fizz.run();
                currentNum++;
            } else {
                Thread.yield();
            }
        }
    }

    private void fizzbuzz(Runnable fizz) throws InterruptedException {
        while (currentNum <= n) {
            if (currentNum % 3 == 0 && currentNum % 5 == 0 && currentNum != 0) {
                fizz.run();
                currentNum++;
            } else {
                Thread.yield();
            }
        }
    }

    private void number(IntConsumer printNumber) throws InterruptedException {
        while (currentNum <= n) {
            if ((currentNum % 3 != 0 && currentNum % 5 != 0)||currentNum == 0) {
                printNumber.accept(currentNum);
                currentNum++;
            } else {
                Thread.yield();
            }
        }
    }*/
    public void fizz(Runnable printFizz) throws InterruptedException {
        for (int i = 3; i <= n; i += 3) {   //只输出3的倍数(不包含15的倍数)
            if (i % 15 == 0) continue;   //15的倍数不处理，交给fizzbuzz()方法处理
            while (state != 3)
                Thread.yield();

            printFizz.run();
            state = 0;
        }
    }

    public void buzz(Runnable printBuzz) throws InterruptedException {
        for (int i = 5; i <= n; i += 5) {   //只输出5的倍数(不包含15的倍数)
            if (i % 15 == 0)    //15的倍数不处理，交给fizzbuzz()方法处理
                continue;
            while (state != 5)
                Thread.yield();
            printBuzz.run();
            state = 0;
        }
    }

    public void fizzbuzz(Runnable printFizzBuzz) throws InterruptedException {
        for (int i = 15; i <= n; i += 15) {   //只输出15的倍数
            while (state != 15)
                Thread.yield();
            printFizzBuzz.run();
            state = 0;
        }
    }

    public void number(IntConsumer printNumber) throws InterruptedException {
        for (int i = 1; i <= n; ++i) {
            while (state != 0)
                Thread.yield();
            if (i % 3 != 0 && i % 5 != 0)
                printNumber.accept(i);
            else {
                if (i % 15 == 0)
                    state = 15;    //交给fizzbuzz()方法处理
                else if (i % 5 == 0)
                    state = 5;    //交给buzz()方法处理
                else
                    state = 3;    //交给fizz()方法处理
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        FizzBuzz fizzBuzz = new FizzBuzz(30);
        Runnable fizz = () -> System.out.println("fizz");
        Runnable buzz = () -> System.out.println("buzz");
        Runnable fizzbuzz = () -> System.out.println("fizzbuzz");
        IntConsumer printNumber = System.out::println;
        new Thread(() -> {
            try {
                fizzBuzz.fizz(fizz);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                fizzBuzz.buzz(buzz);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                fizzBuzz.fizzbuzz(fizzbuzz);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        new Thread(() -> {
            try {
                fizzBuzz.number(printNumber);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

}
