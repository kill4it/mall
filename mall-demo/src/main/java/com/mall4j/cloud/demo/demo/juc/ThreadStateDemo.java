package com.mall4j.cloud.demo.demo.juc;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/9/8 10:27
 * @Version 1.0.0
 **/
public class ThreadStateDemo {
    private static Object lock;
    public static void main(String[] args) throws InterruptedException {
        //NEW
        Thread thread = new Thread();
        System.out.println(thread.getState());
        //Runable-->Running
        thread.start();
        //Block
        Thread t = new Thread(new Runnable (){
            @Override
            public void run() {
                synchronized (lock) { // 阻塞于这里，变为Blocked状态
                    // dothings
                    try {
                        lock.wait();//释放锁，进入另一种Bolck状态
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
    
                }
            }
        });
        t.getState(); //新建之前，还没开始调用start方法，处于New状态
    
        t.start(); //调用start方法，就会进入Runnable状态
        
        Thread.sleep(1000);
    
        while (thread.isAlive()){
            System.out.println("Thread is alive！");
        }
        //Dead
        thread.getState();;
    }
}
