package com.mall4j.cloud.demo.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Desc 要求用三个线程，线程1先打印A,然后是线程2打印B, 然后是线程3打印C. 接着再由线程1打印A....以此类推，打印10次ABC
 */
public class ABCPrinter {

    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        ExecutorService threadPool = Executors.newFixedThreadPool(3);

        threadPool.execute(new MyRunnable1("A", lock, 0));
        threadPool.execute(new MyRunnable1("B", lock, 1));
        threadPool.execute(new MyRunnable1("C", lock, 2));
        threadPool.shutdown();
    }
}

class MyRunnable1 implements Runnable {

    private final String str; //线程打印的字符

    private final Lock lock;

    private final int threadId;

    private static int count = 0; //已打印的次数

    private static final int MAX = 30;

    public MyRunnable1(String str, Lock lock, int threadId) {
        this.str = str;
        this.lock = lock;
        this.threadId = threadId;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (lock) {
                // 为什么在这加判断，而不是在while里面写count<=MAX，因为那样的话会打印11次。
                if (count >= MAX) {
                    return;
                }
                if (count % 3 == threadId) {
                    System.out.println(str);
                    count++;
                }
            }
        }
    }
}
