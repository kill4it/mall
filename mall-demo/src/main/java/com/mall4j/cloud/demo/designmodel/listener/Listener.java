package com.mall4j.cloud.demo.designmodel.listener;

/**
 * 监听器接口
 * @Auther LPJ
 * @Description
 * @Data 2022/12/25 0:06
 * @Version 1.0.0
 **/
public interface Listener {
    void onClick(Event event);
}
