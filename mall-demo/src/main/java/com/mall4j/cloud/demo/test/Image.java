package com.mall4j.cloud.demo.test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/9/14 16:40
 * @Version 1.0.0
 **/
public class Image {
    public static void main(String[] args) throws Exception {
//        HashMap<Object, Object> objectObjectHashMap = new HashMap<>();
//        objectObjectHashMap.put(null,null);
//        ConcurrentHashMap<Object, Object> objectObjectConcurrentHashMap = new ConcurrentHashMap<>();
//        objectObjectConcurrentHashMap.put(null,null);
////        String text = getText("src/test/test.png");
////        System.out.println(text);
////        System.out.println(MD5(text));
    
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
        Calendar c = Calendar.getInstance();
    
        c.add(Calendar.DATE, - 7);
    
        Date time = c.getTime();
    
        String preDay = sdf.format(time);
    
        System.out.println(preDay);
    }
    
    
    public static String getText(String image) {
        File file = new File(image);
        BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageIO.read(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        int minx = 0;
        int miny = 0;
        byte[] bytes = new byte[height * width / 8];
        int x = 0;
        StringBuilder tmp = new StringBuilder();
        for (int j = miny; j < height; j++) {
            for (int i = minx; i < width; i++) {
                int rgb = bufferedImage.getRGB(i, j); //获取对应的像素点
                if (rgb == -1) {
                    tmp.append('0');
                } else {
                    tmp.append('1');
                }
                if (tmp.length() == 8) {
                    bytes[x++] = (byte) Integer.parseInt(tmp.toString(), 2);
                    tmp.setLength(0);
                }
            }
        }
        int i = 0;
        int num = 0;
        int j = 0;
        Boolean isOver = Boolean.FALSE;
        for (; i < bytes.length; i++) {
            num = bytes[i];
            j = 0;
            //判断是否为结束标志
            while (num == 0) {
                num = bytes[i + j];
                j++;
                if (j == 4) {
                    isOver = Boolean.TRUE;
                    break;
                }
            }
            if (isOver) {
                break;
            }
        }
        return new String(bytes, 0, i, StandardCharsets.UTF_8);
    }
    
    private static String MD5(String input) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.update(input.getBytes());
        byte[] byteArray = messageDigest.digest();
        StringBuilder sb = new StringBuilder();
        for (byte b : byteArray) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }
    
    
}
