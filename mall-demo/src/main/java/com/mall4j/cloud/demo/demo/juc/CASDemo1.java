package com.mall4j.cloud.demo.demo.juc;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 通过原子类实现自旋锁
 *
 * @Auther LPJ
 * @Description
 * @Data 2022/9/8 10:39
 * @Version 1.0.0
 **/
public class CASDemo1 {
    AtomicReference<Thread> atomicReference = new AtomicReference<>();
    
    /**
     * 自选方式获取锁
     */
    public void MyCASLock() {
        System.out.println(Thread.currentThread().getName() + "尝试获取锁！");
        //自旋
        while (!atomicReference.compareAndSet(null, Thread.currentThread())) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "自旋获取中！");
        }
        System.out.println(Thread.currentThread().getName() + "锁获取成功！");
    }
    
    /**
     * 指定自旋次数
     */
    public Boolean CountCASLock(Integer count) {
        while (!atomicReference.compareAndSet(null, Thread.currentThread())) {
            if (count == 0) {
                //重试次数到了
                return Boolean.FALSE;
            }
            try {
                Thread.sleep(1000);
                count--;
                System.out.println(Thread.currentThread().getName() + "自旋获取中！");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            
        }
        System.out.println(Thread.currentThread().getName() + "锁获取成功！");
        return Boolean.TRUE;
    }
    
    
    /**
     * 解锁
     */
    public void MyCASUnLock() {
        System.out.println(Thread.currentThread().getName() + "尝试解锁！");
        atomicReference.compareAndSet(Thread.currentThread(), null);
        System.out.println(Thread.currentThread().getName() + "解锁成功！");
    }
    
    
    public static void main(String[] args) {
        CASDemo1 casDemo1Lock = new CASDemo1();
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(5,
                10,
                60,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(20),
                new ThreadPoolExecutor.CallerRunsPolicy());
        poolExecutor.submit(new Runnable() {
            @Override
            public void run() {
                casDemo1Lock.MyCASLock();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                casDemo1Lock.MyCASUnLock();
            }
        });
        poolExecutor.submit(new Runnable() {
            @Override
            public void run() {
                casDemo1Lock.MyCASLock();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                casDemo1Lock.MyCASUnLock();
            }
        });
        poolExecutor.submit(new Runnable() {
            @Override
            public void run() {
                casDemo1Lock.MyCASLock();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                casDemo1Lock.MyCASUnLock();
            }
        });
    }
    
}
