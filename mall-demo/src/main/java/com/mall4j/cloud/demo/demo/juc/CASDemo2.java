package com.mall4j.cloud.demo.demo.juc;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 多线程使用原子类保证操作正确性
 *
 * @Auther LPJ
 * @Description
 * @Data 2022/9/8 17:38
 * @Version 1.0.0
 **/
public class CASDemo2 {
    private static AtomicInteger atomicInteger = new AtomicInteger(0);
    
    public static void main(String[] args) throws InterruptedException {
        //1000累加
        testIAdd();
    }
    
    private static void testIAdd() throws InterruptedException {
        //创建线程池
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        
        for (int i = 0; i < 1000; i++) {
            executorService.execute(() -> {
                
                //自增并返回当前值
                int andIncrement = atomicInteger.incrementAndGet();
                System.out.println("线程:" + Thread.currentThread().getName() + " count=" + andIncrement);
            });
        }
    
        executorService.shutdown();
        Thread.sleep(100);
        System.out.println("最终结果是 ：" + atomicInteger.get());
    }
    
}
