package com.mall4j.cloud.demo.demo.proxy.jdk;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/9/13 18:52
 * @Version 1.0.0
 **/
public class UserServiceImpl implements UserService{
    @Override
    public void addUser() {
        System.out.println("添加用户");
    }
    
    @Override
    public void updateUser(String str) {
        System.out.println("更新用户信息" + str);
    }
    
}
