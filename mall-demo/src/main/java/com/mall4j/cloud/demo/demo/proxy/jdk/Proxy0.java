package com.mall4j.cloud.demo.demo.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.UndeclaredThrowableException;

/**
 * 模拟UserService通过JDK生成的代理对象
 * 1.代理类默认继承Porxy类，因为Java中只支持单继承，所以JDK动态代理只能去实现接口。
 *
 * 2.代理方法都会去调用InvocationHandler的invoke()方法，因此我们需要重写InvocationHandler的invoke()方法。
 *
 * 3.调用invoke()方法时会传入代理实例本身，目标方法和目标方法参数。解释了invoke()方法的参数是怎样来的。
 * @Auther LPJ
 * @Description
 * @Data 2022/9/13 22:39
 * @Version 1.0.0
 **/
public class Proxy0 extends Proxy implements UserService {
    //第一步, 生成构造器
    protected Proxy0(InvocationHandler h) {
        super(h);
    }
    
    //第二步, 生成静态域
    private static Method m1;   //hashCode方法
    private static Method m2;   //equals方法
    private static Method m3;   //toString方法
    private static Method m4;   //addUser方法
    private static Method m5;   //构造方法
    
    //第三步, 生成代理方法
    @Override
    public int hashCode() {
        try {
            return (int) h.invoke(this, m1, null);
        } catch (Throwable e) {
            throw new UndeclaredThrowableException(e);
        }
    }
    
    @Override
    public boolean equals(Object obj) {
        try {
            Object[] args = new Object[] {obj};
            return (boolean) h.invoke(this, m2, args);
        } catch (Throwable e) {
            throw new UndeclaredThrowableException(e);
        }
    }
    
    @Override
    public String toString() {
        try {
            return (String) h.invoke(this, m3, null);
        } catch (Throwable e) {
            throw new UndeclaredThrowableException(e);
        }
    }
    
    @Override
    public void addUser() {
        try {
            //创建参数数组, 如果有多个参数往后面添加就行了
            Object[] args = new Object[] {};
            h.invoke(this, m4, args);
        } catch (Throwable e) {
            throw new UndeclaredThrowableException(e);
        }
    }
    
    @Override
    public void updateUser(String str) {
        try {
            //创建参数数组, 如果有多个参数往后面添加就行了
            Object[] args = new Object[] {str};
            h.invoke(this, m4, args);
        } catch (Throwable e) {
            throw new UndeclaredThrowableException(e);
        }
    }
    
    //第四步, 生成静态初始化方法
    static {
        //获取对应的class类
        try {
            Class objectClass = Class.forName(Object.class.getName());
            Class userServiceClass = Class.forName(UserService.class.getName());
            //通过反射 根据方法名获取方法对应的Method对象
            m1 = objectClass.getMethod("hashCode", null);
            m2 = objectClass.getMethod("equals", new Class[]{Object.class});
            m3 = objectClass.getMethod("toString", null);
            m4 = userServiceClass.getMethod("addUser", null);
            m5 = userServiceClass.getMethod("updateUser",new Class[]{String.class});
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
    
}
