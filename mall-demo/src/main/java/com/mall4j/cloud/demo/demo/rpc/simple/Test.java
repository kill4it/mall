package com.mall4j.cloud.demo.demo.rpc.simple;


import com.mall4j.cloud.demo.demo.rpc.simple.consumer.Client;
import com.mall4j.cloud.demo.demo.rpc.simple.consumer.EchoService;

/**
 * 以上就是一个最简单的 RPC 框架，不包含负载均衡，注册中心，服务治理，集群容错等高级用法。
 * 主要运用 socket、bio、动态代理、反射等基本知识点，实现 RPC 的基本功能，远程过程调用。
 * @Auther LPJ
 * @Description
 * @Data 2022/9/26 19:42
 * @Version 1.0.0
 **/
public class Test {
    /**
     * 本地调用远程服务
     *
     * @param args
     */
    public static void main(String[] args) {
        //此处可以利用FactoryBean创建一个代理类并注册到spring容器中
        EchoService echoService = (EchoService) Client.rpc(EchoService.class);
        //通过依赖注入的方式获取EchoService bean实例
        //如同调本地接口
        String str = echoService.echo("hello");
        System.out.println("本地输出远程调用结果：\n" + str);
    }
}
