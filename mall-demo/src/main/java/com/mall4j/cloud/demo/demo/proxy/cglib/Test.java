package com.mall4j.cloud.demo.demo.proxy.cglib;

import net.sf.cglib.core.DebuggingClassWriter;
import net.sf.cglib.proxy.Enhancer;

public class Test {

    public static void main(String[] args){
        // 获取当前项目的根目录
//        String userDir = System.getProperty("user.dir");
//        //System.setProperty("cglib.debugLocation", userDir);
//        System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, userDir);
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(UserDao.class);
        enhancer.setCallback(new CglibSqlInterceptor(new UserDao()));
        UserDao userDaoProxy = (UserDao) enhancer.create();
        userDaoProxy.save();
    }

}
