package com.mall4j.cloud.demo.demo.juc;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 一些常见的线程池类型
 * newFixedThreadPool (固定数目线程的线程池)
 * newCachedThreadPool(可缓存线程的线程池)
 * newSingleThreadExecutor(单线程的线程池)
 * newScheduledThreadPool(定时及周期执行的线程池)
 *
 * @Auther LPJ
 * @Description
 * @Data 2022/9/8 19:20
 * @Version 1.0.0
 **/
public class ThreadPoolTypeDemo {
    
    /**
     * 核心线程数和最大线程数大小一样
     * 没有所谓的非空闲时间，即keepAliveTime为0
     * 阻塞队列为无界队列LinkedBlockingQueue
     * 使用场景：
     * FixedThreadPool 适用于处理CPU密集型的任务，线程不会销毁，确保CPU在长期被工作线程使用的情况下，尽可能的少的分配线程，即适用执行长期的任务。
     */
    ExecutorService fixedThreadPool = Executors.newFixedThreadPool(10);
    /**
     * 核心线程数为0
     * 最大线程数为Integer.MAX_VALUE
     * 阻塞队列是SynchronousQueue
     * 非核心线程空闲存活时间为60秒
     * 使用场景：
     * 当提交任务的速度大于处理任务的速度时，每次提交一个任务，就必然会创建一个线程。极端情况下会创建过多的线程，耗尽 CPU 和内存资源。由于空闲 60 秒的线程会被终止，长时间保持空闲的 CachedThreadPool 不会占用任何资源。
     */
    ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
    /**
     * 核心线程数为1
     * 最大线程数也为1
     * 阻塞队列是LinkedBlockingQueue
     * keepAliveTime为0
     * 使用场景：
     * 适用于串行执行任务的场景，一个任务一个任务地执行。
     */
    ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
    /**
     * 最大线程数为Integer.MAX_VALUE
     * 阻塞队列是DelayedWorkQueue
     * keepAliveTime为0
     * scheduleAtFixedRate() ：按某种速率周期执行，不用等到上次任务结束
     * scheduleWithFixedDelay()：必须等到上次任务结束才开始计时
     * 使用场景：
     * 周期性执行任务的场景，需要限制线程数量的场景
     * ScheduledExecutorService继承ExecutorService，在普通线程池的功能基础上扩展了定时功能
     */
    ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(10);
    
    public static void main(String[] args) {
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(10);
        //以某个速率执行
        scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + "进来了, " + "当前时间是：" + LocalDateTime.now());
            }
        }, 5L, 2L, TimeUnit.SECONDS);
        
        scheduledExecutorService.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + "进来了, " + "当前时间是：" + LocalDateTime.now());
            }
        },5L, 2L, TimeUnit.SECONDS);
        

    }
    
}
