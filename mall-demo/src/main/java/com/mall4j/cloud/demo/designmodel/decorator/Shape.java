package com.mall4j.cloud.demo.designmodel.decorator;

/**
 * @Auther LPJ
 * @Description
 * @Data 2023/2/8 18:09
 * @Version 1.0.0
 **/
public interface Shape {
    /**
     * 绘制图形
     */
    void draw();
}
