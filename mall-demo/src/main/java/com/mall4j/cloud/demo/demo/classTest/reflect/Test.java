package com.mall4j.cloud.demo.demo.classTest.reflect;

import java.lang.reflect.*;
import java.util.HashMap;

/**
 * JAVA编译时是先获取到类，然
 * 后才是类里边的属性和方法，而反射则和编译相反，他是先获取类里边的对象和方法然后在告诉他是哪个类里的。
 *
 * @Auther LPJ
 * @Description
 * @Data 2022/10/16 10:40
 * @Version 1.0.0
 **/
public class Test {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, NoSuchFieldException, InvocationTargetException, InstantiationException, IllegalAccessException {
        test5();
    }
    
    public static void test1() throws ClassNotFoundException {
        // 通过Object的getClass()方法获取，必须要有对象
        Student s = new Student();
        Class clazz = s.getClass();
        // 通过类名获取字节码对象
        Class clazz2 = Student.class;
        // 通过Class类里的静态方法forName来获取节码对象
        Class clazz3 = Class.forName("demo.classTest.reflect.Student");
        System.out.println(clazz == clazz2);
        System.out.println(clazz == clazz3);
        System.out.println(clazz);
    }
    
    public static void test2() throws NoSuchMethodException, ClassNotFoundException {
        // Class.forName()获取字节码对象
        Class<?> forName = Class.forName("demo.classTest.reflect.Student");
        // 获取所有公共构造方法
        Constructor<?>[] constructors = forName.getConstructors();
        // 遍历
        for (Constructor<?> constructor : constructors) {
            // 打印结果
            System.out.println(constructor);
        }
        System.out.println("--------------------------------------");
        // 暴力获取，可以获取所有的构造方法（包括私有的）
        Constructor<?> c1 = forName.getDeclaredConstructor();
        c1.setAccessible(true);
        System.out.println(c1);
        //获取有参构造
        Constructor<?> c2 = forName.getConstructor(String.class, int.class);
        System.out.println(c2);
    }
    
    public static void test3() throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchFieldException, NoSuchMethodException,
            InvocationTargetException {
        // 获取字节码对象
        Class<?> clazz = Class.forName("demo.classTest.reflect.Student");
        // 创建该类的对象
        Object stu = clazz.newInstance();
        // System.out.println(stu);
        // 获取学生类的name变量
        Field f1 = clazz.getField("name");
        /*
         * set将指定对象变量上此 Field 对象表示的字段设置为指定的新值。
         * 为stu对象里的name变量赋值
         */
        f1.set(stu, "李四");
        // get()返回指定对象上此 Field 表示的字段的值。
        Object name = f1.get(stu);
        // 暴力获取age字段
        Field f2 = clazz.getDeclaredField("age");
        System.out.println(f2);
        // 让jvm不检查权限
        f2.setAccessible(true);
        // 为其赋值
        f2.set(stu, 24);
        // 获取stu对象的f2字段的值
        Object age = f2.get(stu);
        System.out.println(name);
        System.out.println(age);
        // 公有无参无返回值，name()
        Method method = clazz.getMethod("name");
        // 使用Method类的invoke方法执行name方法
        method.invoke(stu);
        // 公有代参无返回值,参数为String类型
        Method m2 = clazz.getMethod("setName", String.class);
        // 执行stu对象的setName方法，传入参数
        m2.invoke(stu, "李晨宇");
        // 公有无参有返回值
        Method m3 = clazz.getMethod("getName");
        // 返回值为invoke
        Object invoke = m3.invoke(stu);
        System.out.println(invoke);
        // 私有
        Method m4 = clazz.getDeclaredMethod("mane1");
        // 让jvm不检查权限
        m4.setAccessible(true);
        // 执行stu对象的mane1方法
        m4.invoke(stu);
    }
    
    public static void test4() throws IllegalAccessException, NoSuchFieldException {
        System.out.println(Student.INT_VALUE);
        Field field = Student.class.getField("INT_VALUE");
        //将字段的访问权限设为true：即去除private修饰符的影响
        field.setAccessible(true);
        //去除final修饰符的影响，将字段设为可修改的
        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        //把字段值设为200
        field.set(null, 200);
        System.out.println(Student.INT_VALUE);
    }
    
    
    public static void test5() throws IllegalAccessException, NoSuchFieldException {
        System.out.println(Student.nums);
        Field field = Student.class.getField("nums");
        field.set(null, 200);
        System.out.println(Student.nums);
    }
    
    public static void test6() throws ReflectiveOperationException {
        // 创建学生01对象
        Student s1 = new Student();
        // 使用MyBeanUtils工具类为学生01对象赋值
        MyBeanUtils.setProrerty(s1, "name", "啦啦");
        MyBeanUtils.setProrerty(s1, "age", 15);
        // 使用MyBeanUtils工具类为学生01对象取值
        String name = MyBeanUtils.getProrerty(s1, "name");
        String age = MyBeanUtils.getProrerty(s1, "age");
        // 打印出来
        System.out.println(name);
        System.out.println(age);
        System.out.println("------------------------------------ ");
        // 创建HashMap作为数据源
        HashMap<String, Object> hashMap = new HashMap<>();
        // 为HashMap赋值
        hashMap.put("qqqqq", "大大的"); // 属性不存在会给出友好型提示
        hashMap.put("name", "大大的");
        hashMap.put("age", 110);
        // 使用MyBeanUtils工具类为学生01对象赋值
        MyBeanUtils.populat(s1, hashMap);
        System.out.println(s1);
    }
}
