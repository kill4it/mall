package com.mall4j.cloud.demo.demo.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/9/13 18:54
 * @Version 1.0.0
 **/
public class UserProxyHandler implements InvocationHandler{
    private Object target;
    
    public UserProxyHandler(Object target) {
        this.target = target;
    }
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object res = method.invoke(target, args);
    
        System.out.println("记录日志");
    
        return res;
    }
    
}
