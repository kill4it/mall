package com.mall4j.cloud.demo.aqs;

import java.util.concurrent.locks.AbstractQueuedSynchronizer;

public class SimpleSharedLock extends AbstractQueuedSynchronizer {
    private static final long serialVersionUID = 1L;

    @Override
    protected int tryAcquireShared(int arg) {
        // 尝试获取共享锁
        // 如果状态为0，则可以获取锁，并返回1表示可以获取锁
        // 如果状态不为0，则可以获取锁，并返回状态+1
        for (;;) {
            int state = getState();
            if (state == 0) {
                if (compareAndSetState(0, 1)) {
                    return 1;
                }
            } else {
                int nextState = state + 1;
                if (compareAndSetState(state, nextState)) {
                    return nextState;
                }
            }
        }
    }
    @Override
    protected boolean tryReleaseShared(int arg) {
        // 释放共享锁
        // 减少状态值，并返回true
        for (;;) {
            int state = getState();
            if (state == 0) {
                throw new IllegalMonitorStateException();
            }
            int nextState = state - 1;
            if (compareAndSetState(state, nextState)) {
                return true;
            }
        }
    }

    public void lock() {
        acquireShared(1);
    }

    public boolean tryLock() {
        return tryAcquireShared(1) >= 0;
    }

    public void unlock() {
        releaseShared(1);
    }
}
