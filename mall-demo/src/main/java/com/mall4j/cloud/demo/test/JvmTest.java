package com.mall4j.cloud.demo.test;

import java.io.*;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * @Auther LPJ
 * @Description
 * @Data 2024/2/23 16:17
 * @Version 1.0.0
 **/
public class JvmTest {
    public static void main(String[] args) {
        //testClassLoader1();
        testClassLoader3();
    }
    
    
    static void testClassLoader1() {
        System.out.println("启动类加载器加载路径：");
        URL[] bootstrapUrls = sun.misc.Launcher.getBootstrapClassPath().getURLs();
        for (URL url : bootstrapUrls) {
            System.out.println(url);
        }
        System.out.println("---------------------------------------");
        System.out.println("扩展类加载器加载路径：");
        URL[] extUrls = ((URLClassLoader) ClassLoader.getSystemClassLoader().getParent()).getURLs();
        for (URL url : extUrls) {
            System.out.println(url);
        }
        System.out.println("---------------------------------------");
        System.out.println("应用类加载器加载路径：");
        URL[] urls = ((URLClassLoader) ClassLoader.getSystemClassLoader()).getURLs();
        for (URL url : urls) {
            System.out.println(url);
        }
    }
    
    static void testClassLoader3() {
        LocalClassLoader localClassLoader = new LocalClassLoader();
        localClassLoader.setRootUrl("/Users/luopeijie/lpjproject/mall/mall-demo/src/main/java/");
        try {
            Class<?> aClass = localClassLoader.findLocalClass("java.lang.String");
            aClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    
    public static class LocalClassLoader extends ClassLoader {
    
        private String rootUrl;
    
        public String getRootUrl() {
            return rootUrl;
        }
    
        public void setRootUrl(String rootUrl) {
            this.rootUrl = rootUrl;
        }
        public Class<?> findLocalClass(String name){
            return findClass(name);
        }
        /**
         * 重写{@link ClassLoader#findClass(String)}方法，通过从外部读取class文件来加载类
         * 因为程序外部class文件均不在前三个类加载器加载范围内，所以最终必然会执行我们的自定义类加载器
         * @param name
         * @return
         */
        @Override
        protected Class<?> findClass(String name){
            try (InputStream inputStream = new FileInputStream(rootUrl + File.separator + name.replace(".", File.separator) + ".class");
                 ByteArrayOutputStream outputStream = new ByteArrayOutputStream()){
                byte[] buffer = new byte[1024];
                int length = 0;
                while ((length = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, length);
                }
                byte[] classBytes = outputStream.toByteArray();
                return defineClass(name, classBytes, 0, classBytes.length);
            }catch (IOException e) {
                e.printStackTrace();
            }
            return null;
    }}
    
    
    
}
