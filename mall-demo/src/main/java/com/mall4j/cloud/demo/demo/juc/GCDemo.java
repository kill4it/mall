package com.mall4j.cloud.demo.demo.juc;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/9/11 16:37
 * @Version 1.0.0
 **/
public class GCDemo {
    public static void main(String[] args) {
//        Object o = new Object();
//        System.out.println(o);
//        System.out.println(o.hashCode());
//        System.gc();
//        System.out.println(o);
//        System.out.println(o.hashCode());
    
        Map map = new HashMap(16); //第一次添加元素数组长度为32
        Map concurrentHashMap = new ConcurrentHashMap<>(16); //第一次添加元素数组长度为64
        concurrentHashMap.put("key","value");
        map.put("key","value");
        System.out.println(concurrentHashMap.size());
        
    }
}
