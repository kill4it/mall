package com.mall4j.cloud.demo.po;

import java.util.Objects;

/**
 * @Auther LPJ
 * @Description
 * @Data 2024/2/19 18:46
 * @Version 1.0.0
 **/
public class UserPo {
    private String name;
    private Double score;
    
    public UserPo(String name, Double score) {
        this.name = name;
        this.score = score;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Double getScore() {
        return score;
    }
    
    public void setScore(Double score) {
        this.score = score;
    }
    
    @Override
    public String toString() {
        return "UserPo{" +
                "name='" + name + '\'' +
                ", score=" + score +
                '}';
    }
}
