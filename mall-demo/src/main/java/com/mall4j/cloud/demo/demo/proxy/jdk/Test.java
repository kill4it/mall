package com.mall4j.cloud.demo.demo.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/9/13 19:05
 * @Version 1.0.0
 **/
public class Test {
    public static void main(String[] args) throws Throwable {
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");

//如果不行可以试下下面这行代码
//System.getProperties().put("jdk.proxy.ProxyGenerator.saveGeneratedFiles", "true");
        //创建一个对象
        UserServiceImpl userServiceimpl = new UserServiceImpl();
        //使用InvocationHandler通过反射来调用方法
        UserProxyHandler userProxyHandler = new UserProxyHandler(userServiceimpl);
        //会生成对应的代理对象也就是Proxy0,Proxy0内部通过UserProxyHandler来调用方法,实现代理
        UserService userService = (UserService)Proxy.newProxyInstance(UserServiceImpl.class.getClassLoader(), UserServiceImpl.class.getInterfaces(), userProxyHandler);
        userService.addUser();
        userService.updateUser("：我是皮皮虾");
        test();
    }
    
    /**
     * 使用InvocationHandler通过反射直接调用对象的方法
     * @throws Throwable
     */
    public static void test() throws Throwable {
        InvocationHandler invocationHandler = new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                System.out.println(method.invoke(proxy,null));
                System.out.println(proxy);
                System.out.println("记录日志");
                return null;
            }
        };
        
        invocationHandler.invoke(new Object(), Object.class.getMethod("hashCode", null), null);
        
    }
}
