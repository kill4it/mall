package com.mall4j.cloud.demo.demo.juc;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/10/10 15:34
 * @Version 1.0.0
 **/
public class ThreadTest {
    
    public static void main(String[] args) throws InterruptedException {
        Thread A = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    System.out.println("A end");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        A.start();
        A.join(1);
        System.out.println("end");
        return;
    }
    

    
}
