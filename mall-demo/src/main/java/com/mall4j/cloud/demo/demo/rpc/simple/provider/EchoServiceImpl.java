package com.mall4j.cloud.demo.demo.rpc.simple.provider;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/9/26 19:48
 * @Version 1.0.0
 **/
public class EchoServiceImpl implements EchoService{
    @Override
    public String echo(String message) {
        return "Hello World";
    }
    
}
