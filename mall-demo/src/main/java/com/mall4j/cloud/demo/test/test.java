package com.mall4j.cloud.demo.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * @Auther LPJ
 * @Description
 * @Data 2023/3/26 19:22
 * @Version 1.0.0
 **/
public class test {

    public static void main(String[] args) {
        // 原始日期字符串
        String originalDateStr = "Mon Sep 09 00:00:00 CST 2024";

        // 定义输入日期格式
        SimpleDateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");

        // 设置时区为中国标准时间
        inputFormat.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));

        // 定义输出日期格式
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            // 将字符串解析为 Date 对象
            Date date = inputFormat.parse(originalDateStr);

            // 将 Date 对象格式化为新的字符串
            String formattedDateStr = outputFormat.format(date);

            // 输出结果
            System.out.println("Formatted Date: " + formattedDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
//        Parent p = new Children();
//        System.out.println(p.getName());
//        System.out.println();
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String dateStr = "2019-01-03 10:59:27";
//        Date date = simpleDateFormat.parse(dateStr);
//        System.out.println(date.getTime());
//
//        try {
//            test();
//        }catch (RuntimeException e){
//            System.out.println("catch");
//            return;
//        }finally {
//            System.out.println("finally");
//        }
//        System.out.println("end");





    public static void test(){
        System.out.println("test");
        throw new RuntimeException();

    }
    
}
