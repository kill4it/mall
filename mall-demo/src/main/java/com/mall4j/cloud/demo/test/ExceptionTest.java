package com.mall4j.cloud.demo.test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExceptionTest {


    public static  void test(){
        throw new NullPointerException();
    }


    public static void main(String[] args) {
//        try {
//            test();
//        }catch (Exception e){
//            System.out.println(e);
//        }
        ExecutorService executorService = Executors.newFixedThreadPool(3);

        for (int i = 0; i < 3; i++) {
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName());
                }
            });
        }
    }


}
