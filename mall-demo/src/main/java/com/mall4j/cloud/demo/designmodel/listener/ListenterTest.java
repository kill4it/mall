package com.mall4j.cloud.demo.designmodel.listener;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/12/25 0:12
 * @Version 1.0.0
 **/
public class ListenterTest {
    
    public static void main(String[] args) {
    //核心思想通过事件event进行关联从而进行解耦
        Listener listenerA = new ListenerA();
        Listener listenerB = new ListenerB();
        ListenerSupport listenerSupport = new ListenerSupport();
        listenerSupport.addListener(listenerA);
        listenerSupport.addListener(listenerB);
        listenerSupport.triggerEvent(new Event("dataA", "typeA"));
    }
}
