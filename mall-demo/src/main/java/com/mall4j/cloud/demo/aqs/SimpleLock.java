package com.mall4j.cloud.demo.aqs;

import java.util.concurrent.locks.AbstractQueuedSynchronizer;

public class SimpleLock extends AbstractQueuedSynchronizer {
    private static final long serialVersionUID = 1L;

    @Override
    protected boolean tryAcquire(int arg) {
        // 尝试获取同步状态
        // 如果状态为0并且成功设置为1，则返回true
        // 否则返回false
        return compareAndSetState(0, 1);
    }

    @Override
    protected boolean tryRelease(int arg) {
        // 释放同步状态
        // 将状态设置为0
        setState(0);
        return true;
    }

    public void lock() {
        acquire(1);
    }

    public boolean tryLock() {
        return tryAcquire(1);
    }

    public void unlock() {
        release(1);
    }
}
