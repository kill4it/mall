package com.mall4j.cloud.demo.demo.juc;

import java.util.concurrent.locks.LockSupport;

/**
 * LockSupport是一个工具类。它的主要作用是挂起和唤醒线程。该工具类是创建锁和其他同步类的基础
 * @Auther LPJ
 * @Description
 * @Data 2022/9/8 17:49
 * @Version 1.0.0
 **/
public class LockSupportTest {
    private static Object object = new Object();
    static MyThread thread = new MyThread("线程田螺");
    
    public static class MyThread extends Thread {
        
        public MyThread(String name) {
            super(name);
        }
        
        @Override
        public void run() {
            synchronized (object) {
                System.out.println("线程名字： " + Thread.currentThread());
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LockSupport.park(); //加锁过程中被终断会继续执行
                
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("线程被中断了");
                }
                System.out.println("继续执行");
            }
        }
    }
    
    public static void main(String[] args) throws InterruptedException {
        thread.start();
        Thread.sleep(5000L);
        thread.interrupt();
        System.out.println("恢复线程调用");
    }
}
