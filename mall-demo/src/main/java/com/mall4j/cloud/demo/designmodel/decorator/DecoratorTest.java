package com.mall4j.cloud.demo.designmodel.decorator;

/**
 * @Auther LPJ
 * @Description
 * @Data 2023/2/8 18:14
 * @Version 1.0.0
 **/
public class DecoratorTest {
    public static void main(String[] args) {
        //正方形 红色 有阴影
        Shape shape = new Square();
        shape = new Red(shape);
        shape = new Shadow(shape);
        shape.draw();
        
        //圆形 绿色
        shape = new Circle();
        shape = new Green(shape);
        shape.draw();
        
        //三角形 蓝色 有阴影
        shape = new Trilateral();
        shape = new Blue(shape);
        shape = new Shadow(shape);
        shape.draw();
    }
}
