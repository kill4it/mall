package com.mall4j.cloud.demo.demo.io.BIO;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * @Auther LPJ
 * @Description
 * @Data 2022/7/31 18:34
 * @Version 1.0.0
 **/
public class BIOClient {
    public static void main(String[] args) throws IOException {
        //要和谁进行通信 server的ip+端口
        Socket client = new Socket("localhost", 8087);
        //获取输入流
        InputStream inputStream = client.getInputStream();
        byte[] buff = new byte[1024];
        int count = 5;
        //将数据读取到内存
        while (true && count > 0) {
            int read = inputStream.read(buff);
            if (read > 0) {
                String s = new String(buff, 0, read, StandardCharsets.UTF_8);
                System.out.println(s);
            }
        }
        client.close();
//        //输出流
//        OutputStream outputStream = client.getOutputStream();
//        String s = UUID.randomUUID().toString();
//        System.out.println("客户端发送" + s);
//        outputStream.write(s.getBytes(StandardCharsets.UTF_8));
//        outputStream.close();
        
    
    }
    
}
