package com.mall4j.cloud.demo.test;

/**
 * @Auther LPJ
 * @Description
 * @Data 2023/3/26 19:21
 * @Version 1.0.0
 **/
public class Children extends Parent{
    
    private String name = "儿子";
    public Integer money = 0;
    
    @Override
    public String getName() {
        return name;
    }
    
    @Override
    public Integer getMoney() {
        return money;
    }
    
    @Override
    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public void setMoney(Integer money) {
        this.money = money;
    }
}
