package com.mall4j.cloud.test.blockchain;

import com.mall4j.cloud.test.blockchain.contract.Hello_sol_hello;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.gas.StaticGasProvider;

import java.math.BigInteger;

public class DeployContract {
    public static void main(String[] args) throws Exception {
        // 连接到以太坊节点
        Web3j web3j = Web3j.build(new HttpService("https://mainnet.infura.io/v3/YOUR_INFURA_PROJECT_ID"));

        // 加载钱包凭证
        String privateKey = "YOUR_PRIVATE_KEY";
        Credentials credentials = Credentials.create(privateKey);

        // 创建合约对象
        RemoteCall<Hello_sol_hello> deploy = Hello_sol_hello.deploy(web3j, credentials, BigInteger.valueOf(1000000000), BigInteger.valueOf(1000000000));
        Hello_sol_hello contract = deploy.send();

        // 获取合约地址
        System.out.println("合约部署成功，地址: " + contract.getContractAddress());
    }
}
