import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicInteger;

public class FileDownloaderTest {


    public static void test() {
        Thread thread1 = new Thread(() -> {
            System.out.println("Thread 1 is running...");
            try {
                Thread.sleep(2000); // 模拟耗时操作
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread 1 finished.");
        });
        Thread thread2 = new Thread(() -> {
            System.out.println("Thread 2 is running...");
            try {
                Thread.sleep(2000); // 模拟耗时操作
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread 2 finished.");
        });
        thread1.start();
        thread2.start();
        try {
            // 等待thread1执行完毕
            thread1.join();
            // 等待thread2执行完毕
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Main thread finished.");
    }

    public static void main(String[] args) {
        test2();
    }

    private static final AtomicInteger counter = new AtomicInteger(0);

    public static void test2() {
        // 尝试将counter的值从0更新为5
        boolean success = counter.compareAndSet(0, 5);
        if (success) {
            System.out.println("Update successful. New value: " + counter.get());
        } else {
            System.out.println("Update failed. Current value: " + counter.get());
        }
    }
}
